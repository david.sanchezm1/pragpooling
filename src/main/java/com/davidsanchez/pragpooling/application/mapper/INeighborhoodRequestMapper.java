package com.davidsanchez.pragpooling.application.mapper;

import com.davidsanchez.pragpooling.application.dto.request.NeighborhoodRequestDto;
import com.davidsanchez.pragpooling.domain.model.Neighborhood;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface INeighborhoodRequestMapper {

    Neighborhood toNeighborhood(NeighborhoodRequestDto neighborhoodRequestDto);
}
