package com.davidsanchez.pragpooling.application.mapper;

import com.davidsanchez.pragpooling.application.dto.response.RideResponseDto;
import com.davidsanchez.pragpooling.domain.model.Ride;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IRideResponseMapper {

    RideResponseDto toResponse(Ride route);

    List<RideResponseDto> toResponseList(List<Ride> rideList);
}
