package com.davidsanchez.pragpooling.application.mapper;

import com.davidsanchez.pragpooling.application.dto.response.NeighborhoodResponseDto;
import com.davidsanchez.pragpooling.domain.model.Neighborhood;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface INeighborhoodResponseMapper {

    NeighborhoodResponseDto toResponse(Neighborhood neighborhood);

    List<NeighborhoodResponseDto> toResponseList(List<Neighborhood> neighborhoodList);
}
