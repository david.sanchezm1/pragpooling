package com.davidsanchez.pragpooling.application.mapper;

import com.davidsanchez.pragpooling.application.dto.request.RideRequestDto;
import com.davidsanchez.pragpooling.domain.model.AvailableRide;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IRideRequestMapper {
    AvailableRide toAvailableRide(RideRequestDto rideRequestDto);
}
