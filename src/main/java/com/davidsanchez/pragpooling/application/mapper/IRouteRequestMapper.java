package com.davidsanchez.pragpooling.application.mapper;

import com.davidsanchez.pragpooling.application.dto.request.RouteRequestDto;
import com.davidsanchez.pragpooling.domain.model.SaveRoute;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IRouteRequestMapper {

    SaveRoute toCreateRoute(RouteRequestDto routeRequestDto);
}
