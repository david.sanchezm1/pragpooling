package com.davidsanchez.pragpooling.application.dto.response;

import com.davidsanchez.pragpooling.domain.model.value_object.RouteGetRide;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class RideResponseDto {
    private Long id;
    private Timestamp departureTime;
    private Integer availableSeats;
    private String meetingPoint;
    private Integer stops;
    private Long routeId;
    private RouteGetRide routeByRouteId;
}
