package com.davidsanchez.pragpooling.application.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NeighborhoodRequestDto {
    private String name;
    private String description;
}
