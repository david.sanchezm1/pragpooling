package com.davidsanchez.pragpooling.application.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NeighborhoodResponseDto {
    private Long id;
    private String name;
}
