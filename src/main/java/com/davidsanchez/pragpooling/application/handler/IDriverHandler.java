package com.davidsanchez.pragpooling.application.handler;

import com.davidsanchez.pragpooling.application.dto.request.RouteRequestDto;
import com.davidsanchez.pragpooling.application.dto.response.RouteResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.infrastructure.security.UserAuthPrincipal;

import java.util.List;

public interface IDriverHandler {
    StringResponseDto saveRoute(RouteRequestDto routeRequestDto, UserAuthPrincipal user);

    List<RouteResponseDto> getAllRoutes(UserAuthPrincipal user);
}