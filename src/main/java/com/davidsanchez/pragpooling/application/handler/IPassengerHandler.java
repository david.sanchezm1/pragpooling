package com.davidsanchez.pragpooling.application.handler;

import com.davidsanchez.pragpooling.application.dto.request.RideRequestDto;
import com.davidsanchez.pragpooling.application.dto.response.RideResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.infrastructure.security.UserAuthPrincipal;

import java.util.List;

public interface IPassengerHandler {
    StringResponseDto joinRide(Long rideId, UserAuthPrincipal user);

    StringResponseDto leaveRoute(UserAuthPrincipal user);

    List<RideResponseDto> getAvailableRides(UserAuthPrincipal user, RideRequestDto rideRequestDto);
}