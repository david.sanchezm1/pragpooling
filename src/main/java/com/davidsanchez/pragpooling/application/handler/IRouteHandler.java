package com.davidsanchez.pragpooling.application.handler;

import com.davidsanchez.pragpooling.application.dto.response.RouteResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;

import java.util.List;

public interface IRouteHandler {
    List<RouteResponseDto> getAllRoutes();

    RouteResponseDto getRouteById(Long routeId);

    StringResponseDto deleteRoute(Long routeId);
}