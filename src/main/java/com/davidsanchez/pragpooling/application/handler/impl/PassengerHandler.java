package com.davidsanchez.pragpooling.application.handler.impl;

import com.davidsanchez.pragpooling.application.dto.request.RideRequestDto;
import com.davidsanchez.pragpooling.application.dto.response.RideResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.application.handler.IPassengerHandler;
import com.davidsanchez.pragpooling.application.mapper.ICurrentUserRequestMapper;
import com.davidsanchez.pragpooling.application.mapper.IMessageResponseMapper;
import com.davidsanchez.pragpooling.application.mapper.IRideRequestMapper;
import com.davidsanchez.pragpooling.application.mapper.IRideResponseMapper;
import com.davidsanchez.pragpooling.application.mapper.IRouteResponseMapper;
import com.davidsanchez.pragpooling.domain.api.IPassengerServicePort;
import com.davidsanchez.pragpooling.domain.model.CurrentUser;
import com.davidsanchez.pragpooling.domain.model.Ride;
import com.davidsanchez.pragpooling.infrastructure.security.UserAuthPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class PassengerHandler implements IPassengerHandler {

    private final IPassengerServicePort passengerServicePort;
    private final IMessageResponseMapper stringResponseMapper;
    private final ICurrentUserRequestMapper currentUserRequestMapper;

    private final IRideRequestMapper rideRequestMapper;
    private final IRideResponseMapper rideResponseMapper;

    private final IRouteResponseMapper routeResponseMapper;

    @Override
    public StringResponseDto joinRide(Long pathId, UserAuthPrincipal user) {
        CurrentUser currentUser = currentUserRequestMapper.toCurrentUser(user);
        return stringResponseMapper.toResponse(passengerServicePort.joinRide(pathId, currentUser));
    }

    @Override
    public StringResponseDto leaveRoute(UserAuthPrincipal user) {
        CurrentUser currentUser = currentUserRequestMapper.toCurrentUser(user);
        return stringResponseMapper.toResponse(passengerServicePort.leaveRoute(currentUser));
    }

    @Override
    public List<RideResponseDto> getAvailableRides(UserAuthPrincipal user, RideRequestDto rideRequestDto) {
        CurrentUser currentUser = currentUserRequestMapper.toCurrentUser(user);
        List<Ride> rideList = passengerServicePort.getAvailableRides(currentUser,
                rideRequestMapper.toAvailableRide(rideRequestDto));

        return rideResponseMapper.toResponseList(rideList);
    }
}