package com.davidsanchez.pragpooling.application.handler.impl;

import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.application.dto.request.UserLoginRequestDto;
import com.davidsanchez.pragpooling.application.dto.response.UserLoginResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.UserResponseDto;
import com.davidsanchez.pragpooling.application.dto.request.UserSignupRequestDto;
import com.davidsanchez.pragpooling.application.handler.IUserHandler;
import com.davidsanchez.pragpooling.application.mapper.IMessageResponseMapper;
import com.davidsanchez.pragpooling.application.mapper.IUserRequestMapper;
import com.davidsanchez.pragpooling.application.mapper.IUserResponseMapper;
import com.davidsanchez.pragpooling.domain.api.IUserServicePort;
import com.davidsanchez.pragpooling.domain.model.CognitoToken;
import com.davidsanchez.pragpooling.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class UserHandler implements IUserHandler {

    private final IUserServicePort userServicePort;
    private final IUserRequestMapper userRequestMapper;
    private final IUserResponseMapper userResponseMapper;
    private final IMessageResponseMapper stringResponseMapper;

    @Override
    public UserLoginResponseDto loginUser(UserLoginRequestDto userLoginRequestDto) {
        User user = userRequestMapper.toUser(userLoginRequestDto);
        CognitoToken cognitoToken = userServicePort.loginUser(user);
        return userResponseMapper.toResponse(cognitoToken);
    }

    @Override
    public StringResponseDto saveUser(UserSignupRequestDto userSignupRequestDto) {
        User user = userRequestMapper.toUser(userSignupRequestDto);
        return stringResponseMapper.toResponse(userServicePort.saveUser(user));
    }

    @Override
    public List<UserResponseDto> getAllUsers() {
        return userResponseMapper.toResponseList(userServicePort.getAllUsers());
    }

    @Override
    public UserResponseDto getUserByEmail(String email) {
        return userResponseMapper.toResponse(userServicePort.getUserByEmail(email));
    }

    @Override
    public void deleteUser(Long userId) {
        userServicePort.deleteUser(userId);
    }
}