package com.davidsanchez.pragpooling.application.handler.impl;

import com.davidsanchez.pragpooling.application.dto.response.RouteResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.application.handler.IRouteHandler;
import com.davidsanchez.pragpooling.application.mapper.IMessageResponseMapper;
import com.davidsanchez.pragpooling.application.mapper.IRouteResponseMapper;
import com.davidsanchez.pragpooling.domain.api.IRouteServicePort;
import com.davidsanchez.pragpooling.domain.model.MessageResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class RouteHandler implements IRouteHandler {

    private final IRouteServicePort routeServicePort;
    private final IRouteResponseMapper routeResponseMapper;
    private final IMessageResponseMapper stringResponseMapper;

    @Override
    public List<RouteResponseDto> getAllRoutes() {
        return routeResponseMapper.toResponseList(routeServicePort.getAllRoutes());
    }

    @Override
    public RouteResponseDto getRouteById(Long routeId) {
        return routeResponseMapper.toResponse(routeServicePort.getRouteById(routeId));
    }

    @Override
    public StringResponseDto deleteRoute(Long routeId) {
        MessageResponse messageResponse = routeServicePort.deleteRoute(routeId);
        return stringResponseMapper.toResponse(messageResponse);
    }
}