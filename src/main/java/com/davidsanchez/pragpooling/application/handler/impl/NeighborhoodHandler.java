package com.davidsanchez.pragpooling.application.handler.impl;

import com.davidsanchez.pragpooling.application.dto.request.NeighborhoodRequestDto;
import com.davidsanchez.pragpooling.application.dto.response.NeighborhoodResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.application.handler.INeighborhoodHandler;
import com.davidsanchez.pragpooling.application.mapper.IMessageResponseMapper;
import com.davidsanchez.pragpooling.application.mapper.INeighborhoodRequestMapper;
import com.davidsanchez.pragpooling.application.mapper.INeighborhoodResponseMapper;
import com.davidsanchez.pragpooling.domain.api.INeighborhoodServicePort;
import com.davidsanchez.pragpooling.domain.model.MessageResponse;
import com.davidsanchez.pragpooling.domain.model.Neighborhood;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class NeighborhoodHandler implements INeighborhoodHandler {

    private final INeighborhoodServicePort neighborhoodServicePort;
    private final INeighborhoodRequestMapper neighborhoodRequestMapper;
    private final INeighborhoodResponseMapper neighborhoodResponseMapper;
    private final IMessageResponseMapper stringResponseMapper;

    @Override
    public StringResponseDto saveNeighborhood(NeighborhoodRequestDto neighborhoodRequestDto) {
        Neighborhood neighborhood = neighborhoodRequestMapper.toNeighborhood(neighborhoodRequestDto);
        MessageResponse messageResponse = neighborhoodServicePort.saveNeighborhood(neighborhood);
        return stringResponseMapper.toResponse(messageResponse);
    }

    @Override
    public List<NeighborhoodResponseDto> getAllNeighborhoods() {
        return neighborhoodResponseMapper.toResponseList(neighborhoodServicePort.getAllNeighborhoods());
    }

    @Override
    public void deleteNeighborhood(Long neighborhoodId) {
        neighborhoodServicePort.deleteNeighborhood(neighborhoodId);
    }
}