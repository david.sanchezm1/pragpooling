package com.davidsanchez.pragpooling.application.handler;

import com.davidsanchez.pragpooling.application.dto.request.NeighborhoodRequestDto;
import com.davidsanchez.pragpooling.application.dto.response.NeighborhoodResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;

import java.util.List;

public interface INeighborhoodHandler {
    StringResponseDto saveNeighborhood(NeighborhoodRequestDto neighborhoodRequestDto);

    List<NeighborhoodResponseDto> getAllNeighborhoods();

    void deleteNeighborhood(Long neighborhoodId);
}