package com.davidsanchez.pragpooling.domain.spi;

import com.davidsanchez.pragpooling.domain.model.CognitoToken;
import com.davidsanchez.pragpooling.domain.model.User;

public interface ICognitoPersistencePort {

    String signupUser(User user);

    CognitoToken loginUser(User user);
}