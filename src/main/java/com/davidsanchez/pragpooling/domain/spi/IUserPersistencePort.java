package com.davidsanchez.pragpooling.domain.spi;

import com.davidsanchez.pragpooling.domain.model.User;

import java.util.List;

public interface IUserPersistencePort {
    User joinRide(User user, Long rideId);

    User leaveRide(User user);

    User saveUser(User user);

    List<User> getAllUsers();

    User getUserByEmail(String email);

    User getUserByUsername(String username);

    User getUserById(Long userId);

    void deleteUser(Long userId);

    void validateEmailUniqueness(String email);

    User getUserByEmailAndPassword(User user);
}