package com.davidsanchez.pragpooling.domain.spi;

import com.davidsanchez.pragpooling.domain.model.AvailableRide;
import com.davidsanchez.pragpooling.domain.model.Ride;

import java.util.List;

public interface IRidePersistencePort {
    Ride getRideById(Long rideId);

    void saveRides(List<Ride> rides);

    void decrementAvailableSeats(Long rideId);

    void incrementAvailableSeats(Long rideId);

    List<Ride> getAll();

    List<Ride> getAllAvailableRides(AvailableRide availableRide);

    void decrementStops(Long rideId);

    void incrementStops(Long rideId);
}