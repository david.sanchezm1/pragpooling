package com.davidsanchez.pragpooling.domain.spi;

import com.davidsanchez.pragpooling.domain.model.Path;

import java.util.List;

public interface IPathPersistencePort {
    Path savePath(Path path);

    List<Path> savePaths(List<Path> pathList);

    List<Path> getAllPaths();

    Path getPathById(Long pathId);
}