package com.davidsanchez.pragpooling.domain.model.value_object;

import java.util.Collection;

public class RouteGetRide {
    private Long id;
    private Integer totalPaths;
    private Long driverId;
    private Collection<PathSlim> pathsByRouteId;

    public RouteGetRide() {
    }

    public RouteGetRide(Long id, Integer totalPaths, Long driverId, Collection<PathSlim> pathsByRouteId) {
        this.id = id;
        this.totalPaths = totalPaths;
        this.driverId = driverId;
        this.pathsByRouteId = pathsByRouteId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalPaths() {
        return totalPaths;
    }

    public void setTotalPaths(Integer totalPaths) {
        this.totalPaths = totalPaths;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public Collection<PathSlim> getPathsByRouteId() {
        return pathsByRouteId;
    }

    public void setPathsByRouteId(Collection<PathSlim> pathsByRouteId) {
        this.pathsByRouteId = pathsByRouteId;
    }
}
