package com.davidsanchez.pragpooling.domain.model.value_object;

public class PathSlim {
    private Long id;
    private Integer position;
    private Long neighborhoodId;
    private Long routeId;
    private NeighborhoodSlim neighborhoodByNeighborhoodId;

    public PathSlim() {
    }

    public PathSlim(Long id, Integer position, Long neighborhoodId, Long routeId, NeighborhoodSlim neighborhoodByNeighborhoodId) {
        this.id = id;
        this.position = position;
        this.neighborhoodId = neighborhoodId;
        this.routeId = routeId;
        this.neighborhoodByNeighborhoodId = neighborhoodByNeighborhoodId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Long getNeighborhoodId() {
        return neighborhoodId;
    }

    public void setNeighborhoodId(Long neighborhoodId) {
        this.neighborhoodId = neighborhoodId;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public NeighborhoodSlim getNeighborhoodByNeighborhoodId() {
        return neighborhoodByNeighborhoodId;
    }

    public void setNeighborhoodByNeighborhoodId(NeighborhoodSlim neighborhoodByNeighborhoodId) {
        this.neighborhoodByNeighborhoodId = neighborhoodByNeighborhoodId;
    }
}