package com.davidsanchez.pragpooling.domain.model;

import java.sql.Timestamp;
import java.util.List;

public class SaveRoute {
    private Integer seats;
    private List<Timestamp> rides;
    private List<Long> neighborhoods;
    private String description;
    private String meetingPoint;

    public SaveRoute() {
    }

    public SaveRoute(Integer seats, List<Timestamp> rides, List<Long> neighborhoods, String description, String meetingPoint) {
        this.seats = seats;
        this.rides = rides;
        this.neighborhoods = neighborhoods;
        this.description = description;
        this.meetingPoint = meetingPoint;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public List<Timestamp> getRides() {
        return rides;
    }

    public void setRides(List<Timestamp> rides) {
        this.rides = rides;
    }

    public List<Long> getNeighborhoods() {
        return neighborhoods;
    }

    public void setNeighborhoods(List<Long> neighborhoods) {
        this.neighborhoods = neighborhoods;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMeetingPoint() {
        return meetingPoint;
    }

    public void setMeetingPoint(String meetingPoint) {
        this.meetingPoint = meetingPoint;
    }
}