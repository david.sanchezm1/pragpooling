package com.davidsanchez.pragpooling.domain.model;

import java.sql.Timestamp;

public class AvailableRide {
    private Long neighborhoodOriginId;
    private Long neighborhoodDestinationId;
    private Timestamp date;

    public AvailableRide() {
    }

    public AvailableRide(Long neighborhoodOriginId, Long neighborhoodDestinationId, Timestamp date) {
        this.neighborhoodOriginId = neighborhoodOriginId;
        this.neighborhoodDestinationId = neighborhoodDestinationId;
        this.date = date;
    }

    public Long getNeighborhoodOriginId() {
        return neighborhoodOriginId;
    }

    public void setNeighborhoodOriginId(Long neighborhoodOriginId) {
        this.neighborhoodOriginId = neighborhoodOriginId;
    }

    public Long getNeighborhoodDestinationId() {
        return neighborhoodDestinationId;
    }

    public void setNeighborhoodDestinationId(Long neighborhoodDestinationId) {
        this.neighborhoodDestinationId = neighborhoodDestinationId;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
