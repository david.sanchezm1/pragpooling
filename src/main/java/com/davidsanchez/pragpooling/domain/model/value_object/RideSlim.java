package com.davidsanchez.pragpooling.domain.model.value_object;

import java.sql.Timestamp;
import java.util.Collection;

public class RideSlim {
    private Long id;
    private Timestamp departureTime;
    private Integer availableSeats;
    private String meetingPoint;
    private Integer stops;
    private Long routeId;
    private Collection<UserSlim> usersByRideId;

    public RideSlim() {
    }

    public RideSlim(Long id, Timestamp departureTime, Integer availableSeats, String meetingPoint, Integer stops, Long routeId, Collection<UserSlim> usersByRideId) {
        this.id = id;
        this.departureTime = departureTime;
        this.availableSeats = availableSeats;
        this.meetingPoint = meetingPoint;
        this.stops = stops;
        this.routeId = routeId;
        this.usersByRideId = usersByRideId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Timestamp departureTime) {
        this.departureTime = departureTime;
    }

    public Integer getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(Integer availableSeats) {
        this.availableSeats = availableSeats;
    }

    public String getMeetingPoint() {
        return meetingPoint;
    }

    public void setMeetingPoint(String meetingPoint) {
        this.meetingPoint = meetingPoint;
    }

    public Integer getStops() {
        return stops;
    }

    public void setStops(Integer stops) {
        this.stops = stops;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public Collection<UserSlim> getUsersByRideId() {
        return usersByRideId;
    }

    public void setUsersByRideId(Collection<UserSlim> usersByRideId) {
        this.usersByRideId = usersByRideId;
    }
}