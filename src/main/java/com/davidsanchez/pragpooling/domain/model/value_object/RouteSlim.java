package com.davidsanchez.pragpooling.domain.model.value_object;

public class RouteSlim {
    private Long id;
    private Integer totalPaths;
    private Long driverId;

    public RouteSlim() {
    }

    public RouteSlim(Long id, Integer totalPaths, Long driverId) {
        this.id = id;
        this.totalPaths = totalPaths;
        this.driverId = driverId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalPaths() {
        return totalPaths;
    }

    public void setTotalPaths(Integer totalPaths) {
        this.totalPaths = totalPaths;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }
}