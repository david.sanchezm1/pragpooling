package com.davidsanchez.pragpooling.domain.model;

import com.davidsanchez.pragpooling.domain.model.value_object.PathSlim;
import com.davidsanchez.pragpooling.domain.model.value_object.RideSlim;

import java.util.Collection;

public class Route {
    private Long id;
    private Integer totalPaths;
    private Long driverId;
    private Collection<PathSlim> pathsByRouteId;
    private Collection<RideSlim> ridesByRouteId;

    public Route() {
    }

    public Route(Long id, Integer totalPaths, Long driverId, Collection<PathSlim> pathsByRouteId, Collection<RideSlim> ridesByRouteId) {
        this.id = id;
        this.totalPaths = totalPaths;
        this.driverId = driverId;
        this.pathsByRouteId = pathsByRouteId;
        this.ridesByRouteId = ridesByRouteId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalPaths() {
        return totalPaths;
    }

    public void setTotalPaths(Integer totalPaths) {
        this.totalPaths = totalPaths;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public Collection<PathSlim> getPathsByRouteId() {
        return pathsByRouteId;
    }

    public void setPathsByRouteId(Collection<PathSlim> pathsByRouteId) {
        this.pathsByRouteId = pathsByRouteId;
    }

    public Collection<RideSlim> getRidesByRouteId() {
        return ridesByRouteId;
    }

    public void setRidesByRouteId(Collection<RideSlim> ridesByRouteId) {
        this.ridesByRouteId = ridesByRouteId;
    }
}
