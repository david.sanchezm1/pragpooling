package com.davidsanchez.pragpooling.domain.exception;

public class PassengerCantBeTheDriverException extends RuntimeException {
    public PassengerCantBeTheDriverException() {
        super();
    }
}
