package com.davidsanchez.pragpooling.domain.exception;

public class PassengerHasNoRideException extends RuntimeException {
    public PassengerHasNoRideException() {
        super();
    }
}
