package com.davidsanchez.pragpooling.domain.exception;

public class NoMoreSeatsAvailableException extends RuntimeException {
    public NoMoreSeatsAvailableException() {
        super();
    }
}
