package com.davidsanchez.pragpooling.domain.usecase;

import com.davidsanchez.pragpooling.domain.api.IDriverServicePort;
import com.davidsanchez.pragpooling.domain.model.CurrentUser;
import com.davidsanchez.pragpooling.domain.model.MessageResponse;
import com.davidsanchez.pragpooling.domain.model.Path;
import com.davidsanchez.pragpooling.domain.model.Ride;
import com.davidsanchez.pragpooling.domain.model.Route;
import com.davidsanchez.pragpooling.domain.model.SaveRoute;
import com.davidsanchez.pragpooling.domain.model.User;
import com.davidsanchez.pragpooling.domain.spi.INeighborhoodPersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IPathPersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IRidePersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IRoutePersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IUserPersistencePort;
import com.davidsanchez.pragpooling.domain.validation.DriverValidator;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class DriverUseCase implements IDriverServicePort {

    private final IRoutePersistencePort routePersistencePort;
    private final IUserPersistencePort userPersistencePort;
    private final IRidePersistencePort ridePersistencePort;
    private final INeighborhoodPersistencePort neighborhoodPersistencePort;
    private final IPathPersistencePort pathPersistencePort;


    public DriverUseCase(IRoutePersistencePort routePersistencePort, IUserPersistencePort userPersistencePort,
                         IRidePersistencePort ridePersistencePort,
                         INeighborhoodPersistencePort neighborhoodPersistencePort, IPathPersistencePort pathPersistencePort) {
        this.routePersistencePort = routePersistencePort;
        this.userPersistencePort = userPersistencePort;
        this.ridePersistencePort = ridePersistencePort;
        this.neighborhoodPersistencePort = neighborhoodPersistencePort;
        this.pathPersistencePort = pathPersistencePort;
    }

    @Override
    public MessageResponse saveRoute(SaveRoute saveRoute, CurrentUser user) {
        // Validate business rules
        DriverValidator.validateSaveRoute(saveRoute);
        neighborhoodPersistencePort.validateIds(saveRoute.getNeighborhoods());

        // Persists and manipulate models
        User driver = userPersistencePort.getUserByUsername(user.getUsername());

        Route route = new Route();
        route.setTotalPaths(saveRoute.getNeighborhoods().size());
        route.setDriverId(driver.getId());

        Route createdRoute = routePersistencePort.saveRoute(route);

        List<Ride> rides = getRideList(saveRoute, createdRoute);
        ridePersistencePort.saveRides(rides);

        List<Path> pathList = getPathList(saveRoute, createdRoute);
        pathPersistencePort.savePaths(pathList);

        return new MessageResponse(String.format("Route created with id %d", createdRoute.getId()));
    }

    @Override
    public List<Route> getAllRoutes(CurrentUser user) {
        User driver = userPersistencePort.getUserByUsername(user.getUsername());
        return routePersistencePort.getAllRoutesByDriverId(driver.getId());
    }

    private static List<Path> getPathList(SaveRoute saveRoute, Route createdRoute) {
        List<Path> pathList = new ArrayList<>();
        int position = 1;
        for (Long neighborhoodId : saveRoute.getNeighborhoods()) {
            Path path = new Path();
            path.setPosition(position);
            path.setRouteId(createdRoute.getId());
            path.setNeighborhoodId(neighborhoodId);
            pathList.add(path);

            position += 1;
        }
        return pathList;
    }

    private List<Ride> getRideList(SaveRoute saveRoute, Route createdRoute) {
        List<Ride> rides = new ArrayList<>();
        for (Timestamp timestamp : saveRoute.getRides()) {
            Ride ride = new Ride();
            ride.setDepartureTime(timestamp);
            ride.setRouteId(createdRoute.getId());
            ride.setStops(0);
            ride.setMeetingPoint(saveRoute.getMeetingPoint());
            ride.setAvailableSeats(saveRoute.getSeats());
            rides.add(ride);
        }
        return rides;
    }
}