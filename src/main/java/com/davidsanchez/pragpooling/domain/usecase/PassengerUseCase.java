package com.davidsanchez.pragpooling.domain.usecase;

import com.davidsanchez.pragpooling.domain.api.IPassengerServicePort;
import com.davidsanchez.pragpooling.domain.exception.NoMoreSeatsAvailableException;
import com.davidsanchez.pragpooling.domain.exception.PassengerCantBeTheDriverException;
import com.davidsanchez.pragpooling.domain.exception.PassengerHasNoRideException;
import com.davidsanchez.pragpooling.domain.model.AvailableRide;
import com.davidsanchez.pragpooling.domain.model.CurrentUser;
import com.davidsanchez.pragpooling.domain.model.MessageResponse;
import com.davidsanchez.pragpooling.domain.model.Ride;
import com.davidsanchez.pragpooling.domain.model.User;
import com.davidsanchez.pragpooling.domain.spi.INeighborhoodPersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IRidePersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IUserPersistencePort;
import com.davidsanchez.pragpooling.domain.validation.RideValidator;

import java.util.List;

public class PassengerUseCase implements IPassengerServicePort {

    private final IUserPersistencePort userPersistencePort;
    private final IRidePersistencePort ridePersistencePort;
    private final INeighborhoodPersistencePort neighborhoodPersistencePort;

    public PassengerUseCase(IUserPersistencePort userPersistencePort, IRidePersistencePort ridePersistencePort,
                            INeighborhoodPersistencePort neighborhoodPersistencePort) {
        this.userPersistencePort = userPersistencePort;
        this.ridePersistencePort = ridePersistencePort;
        this.neighborhoodPersistencePort = neighborhoodPersistencePort;
    }

    @Override
    public MessageResponse joinRide(Long rideId, CurrentUser user) {
        Ride ride = ridePersistencePort.getRideById(rideId);
        User passenger = userPersistencePort.getUserByUsername(user.getUsername());
        Long driverId = ride.getRouteByRouteId().getDriverId();

        if (passenger.getId().equals(driverId)) {
            throw new PassengerCantBeTheDriverException();
        }

        if (ride.getAvailableSeats() > 0) {
            ridePersistencePort.decrementAvailableSeats(rideId);
        } else {
            throw new NoMoreSeatsAvailableException();
        }

        userPersistencePort.joinRide(passenger, rideId);
        ridePersistencePort.incrementStops(rideId);

        return new MessageResponse(String.format("You have joined the ride %d successfully", rideId));
    }

    @Override
    public MessageResponse leaveRoute(CurrentUser user) {
        User passenger = userPersistencePort.getUserByUsername(user.getUsername());
        Long rideId = passenger.getRideId();
        if (rideId == null) {
            throw new PassengerHasNoRideException();
        }

        userPersistencePort.leaveRide(passenger);
        ridePersistencePort.incrementAvailableSeats(rideId);
        ridePersistencePort.decrementStops(rideId);

        return new MessageResponse(String.format("You have left the ride %d successfully", rideId));
    }

    @Override
    public List<Ride> getAvailableRides(CurrentUser user, AvailableRide availableRide) {
        RideValidator.validateAvailableRide(availableRide);
        List<Long> neighborhoodIdList = List.of(
                availableRide.getNeighborhoodDestinationId(),
                availableRide.getNeighborhoodOriginId()
        );
        neighborhoodPersistencePort.validateIds(neighborhoodIdList);

        return ridePersistencePort.getAllAvailableRides(availableRide);
    }
}