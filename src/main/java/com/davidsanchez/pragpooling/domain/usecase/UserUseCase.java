package com.davidsanchez.pragpooling.domain.usecase;

import com.davidsanchez.pragpooling.domain.api.IUserServicePort;
import com.davidsanchez.pragpooling.domain.model.CognitoToken;
import com.davidsanchez.pragpooling.domain.model.MessageResponse;
import com.davidsanchez.pragpooling.domain.model.User;
import com.davidsanchez.pragpooling.domain.spi.ICognitoPersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IUserPersistencePort;
import com.davidsanchez.pragpooling.domain.validation.UserValidator;

import java.util.List;

public class UserUseCase implements IUserServicePort {

    private final IUserPersistencePort userPersistencePort;
    private final ICognitoPersistencePort cognitoPersistencePort;

    public UserUseCase(IUserPersistencePort userPersistencePort, ICognitoPersistencePort cognitoPersistencePort) {
        this.userPersistencePort = userPersistencePort;
        this.cognitoPersistencePort = cognitoPersistencePort;
    }

    @Override
    public CognitoToken loginUser(User user) {
        // Validate business rules
        UserValidator.validateLoginUser(user);

        User authenticatedUser = userPersistencePort.getUserByEmailAndPassword(user);
        return cognitoPersistencePort.loginUser(authenticatedUser);
    }

    @Override
    public MessageResponse saveUser(User user) {
        // Validate business rules
        UserValidator.validateSaveUser(user);
        userPersistencePort.validateEmailUniqueness(user.getEmail());

        // Persists and manipulate models
        String username = cognitoPersistencePort.signupUser(user);
        user.setUsername(username);

        User createdUser = userPersistencePort.saveUser(user);
        return new MessageResponse(String.format("User created with id %d", createdUser.getId()));
    }

    @Override
    public List<User> getAllUsers() {
        return userPersistencePort.getAllUsers();
    }

    @Override
    public User getUserByEmail(String email) {
        return userPersistencePort.getUserByEmail(email);
    }

    @Override
    public void deleteUser(Long userId) {
        userPersistencePort.deleteUser(userId);
    }
}