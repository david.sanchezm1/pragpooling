package com.davidsanchez.pragpooling.domain.usecase;

import com.davidsanchez.pragpooling.domain.api.IRouteServicePort;
import com.davidsanchez.pragpooling.domain.model.MessageResponse;
import com.davidsanchez.pragpooling.domain.model.Route;
import com.davidsanchez.pragpooling.domain.spi.IRoutePersistencePort;

import java.util.List;

public class RouteUseCase implements IRouteServicePort {

    private final IRoutePersistencePort routePersistencePort;

    public RouteUseCase(IRoutePersistencePort routePersistencePort) {
        this.routePersistencePort = routePersistencePort;
    }

    @Override
    public List<Route> getAllRoutes() {
        return routePersistencePort.getAllRoutes();
    }

    @Override
    public Route getRouteById(Long routeId) {
        return routePersistencePort.getRouteById(routeId);
    }

    @Override
    public MessageResponse deleteRoute(Long routeId) {
        routePersistencePort.deleteRoute(routeId);
        return new MessageResponse(String.format("Route with id %d deleted successfully", routeId));
    }
}