package com.davidsanchez.pragpooling.domain.usecase;

import com.davidsanchez.pragpooling.domain.api.INeighborhoodServicePort;
import com.davidsanchez.pragpooling.domain.model.MessageResponse;
import com.davidsanchez.pragpooling.domain.model.Neighborhood;
import com.davidsanchez.pragpooling.domain.spi.INeighborhoodPersistencePort;
import com.davidsanchez.pragpooling.domain.validation.NeighborhoodValidator;

import java.util.List;

public class NeighborhoodUseCase implements INeighborhoodServicePort {
    private final INeighborhoodPersistencePort neighborhoodPersistencePort;

    public NeighborhoodUseCase(INeighborhoodPersistencePort neighborhoodPersistencePort) {
        this.neighborhoodPersistencePort = neighborhoodPersistencePort;
    }

    @Override
    public MessageResponse saveNeighborhood(Neighborhood neighborhood) {
        NeighborhoodValidator.validateNeighborhood(neighborhood);
        Neighborhood savedNeighborhood = neighborhoodPersistencePort.saveNeighborhood(neighborhood);
        return new MessageResponse(String.format("Neighborhood created with id %d", savedNeighborhood.getId()));
    }

    @Override
    public List<Neighborhood> getAllNeighborhoods() {
        return neighborhoodPersistencePort.getAllNeighborhoods();
    }

    @Override
    public void deleteNeighborhood(Long neighborhoodId) {
        neighborhoodPersistencePort.deleteNeighborhood(neighborhoodId);
    }
}