package com.davidsanchez.pragpooling.domain.validation;

import com.davidsanchez.pragpooling.domain.exception.DomainException;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.util.Strings;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtils {

    private ValidationUtils() {

    }

    private static final String PASSWORD_PATTERN =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*_-]).{8,15}$";

    private static final Pattern passwordPattern = Pattern.compile(PASSWORD_PATTERN);

    private static boolean isValidPassword(String s) {
        if (Strings.isBlank(s)) {
            return false;
        }

        Matcher matcher = passwordPattern.matcher(s);
        return matcher.matches();

    }

    private static boolean isValidEmail(String email) {
        EmailValidator validator = EmailValidator.getInstance();
        return validator.isValid(email);
    }

    private static boolean isBlank(final CharSequence cs) {
        final int strLen = length(cs);
        if (strLen == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private static int length(final CharSequence cs) {
        return cs == null ? 0 : cs.length();
    }

    public static <T> T requireNonNull(T obj, String field) {
        if (obj == null) {
            throw new DomainException(field + " should not be null");
        }
        return obj;
    }

    public static int requireBetween(int value, int min, int max, String field) {
        if (!(value >= min && value <= max)) {
            throw new DomainException(field + " should be between " + min + " and " + max);
        }
        return value;
    }

    public static int requireGreaterOrEqualThan(int value, int min, String field) {
        if (value < min) {
            throw new DomainException(field + " should be greater or equal than " + min);
        }
        return value;
    }

    public static void requireNonBlank(String s, String field) {
        if (isBlank(s)) {
            throw new DomainException(field + " should not be blank");
        }
    }

    public static void requireValidEmail(String s, String field) {
        if (!isValidEmail(s)) {
            throw new DomainException(field + " should be a valid email");
        }
    }

    public static void requireValidPassword(String s, String field) {
        if (!isValidPassword(s)) {
            throw new DomainException(field + " must be between 8 and 15 characters long and must contain" +
                    " an uppercase letter, lowercase letter, number, and a special character (*_-)"
            );
        }
    }
}
