package com.davidsanchez.pragpooling.domain.validation;

import com.davidsanchez.pragpooling.domain.model.AvailableRide;

import static com.davidsanchez.pragpooling.domain.validation.ValidationUtils.requireNonNull;

public class RideValidator {

    private RideValidator() {

    }

    public static void validateAvailableRide(final AvailableRide availableRide) {
        requireNonNull(availableRide, "AvailableRide");
        requireNonNull(availableRide.getNeighborhoodOriginId(), "neighborhoodOriginId");
        requireNonNull(availableRide.getNeighborhoodDestinationId(), "neighborhoodDestinationId");
        requireNonNull(availableRide.getDate(), "date");
    }

}
