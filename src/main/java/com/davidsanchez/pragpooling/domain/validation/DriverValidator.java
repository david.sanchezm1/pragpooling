package com.davidsanchez.pragpooling.domain.validation;

import com.davidsanchez.pragpooling.domain.exception.DuplicateNeighborhoodException;
import com.davidsanchez.pragpooling.domain.model.SaveRoute;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.davidsanchez.pragpooling.domain.validation.ValidationUtils.requireBetween;
import static com.davidsanchez.pragpooling.domain.validation.ValidationUtils.requireGreaterOrEqualThan;
import static com.davidsanchez.pragpooling.domain.validation.ValidationUtils.requireNonNull;

public class DriverValidator {

    public static final int MIN_SEATS = 1;
    public static final int MAX_SEATS = 4;
    public static final int MIN_NEIGHBORHOODS = 2;
    public static final int MIN_RIDES = 1;

    private DriverValidator() {

    }

    public static void validateSaveRoute(final SaveRoute saveRoute) {
        requireNonNull(saveRoute, "SaveRoute");
        requireNonNull(saveRoute.getSeats(), "seats");
        requireNonNull(saveRoute.getRides(), "rides");
        requireNonNull(saveRoute.getNeighborhoods(), "neighborhoods");
        requireNonNull(saveRoute.getDescription(), "description");
        requireNonNull(saveRoute.getMeetingPoint(), "meetingPoint");
        requireBetween(saveRoute.getSeats(), MIN_SEATS, MAX_SEATS, "seats");
        requireGreaterOrEqualThan(saveRoute.getNeighborhoods().size(), MIN_NEIGHBORHOODS, "neighborhoods");
        requireGreaterOrEqualThan(saveRoute.getRides().size(), MIN_RIDES, "rides");

        requireNeighborhoodsNotDuplicate(saveRoute.getNeighborhoods());
    }

    public static void requireNeighborhoodsNotDuplicate(List<Long> neighborhoods) {
        Set<Long> hashSet = new HashSet<>(neighborhoods);
        if (hashSet.size() < neighborhoods.size()) {
            throw new DuplicateNeighborhoodException();
        }
    }

}
