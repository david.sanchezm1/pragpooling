package com.davidsanchez.pragpooling.domain.validation;

import com.davidsanchez.pragpooling.domain.model.Neighborhood;

import static com.davidsanchez.pragpooling.domain.validation.ValidationUtils.requireNonBlank;
import static com.davidsanchez.pragpooling.domain.validation.ValidationUtils.requireNonNull;

public class NeighborhoodValidator {

    private NeighborhoodValidator() {

    }

    public static void validateNeighborhood(final Neighborhood neighborhood) {
        requireNonNull(neighborhood, "Neighborhood");
        requireNonBlank(neighborhood.getName(), "name");
    }

}
