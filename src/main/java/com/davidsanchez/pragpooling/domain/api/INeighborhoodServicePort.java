package com.davidsanchez.pragpooling.domain.api;

import com.davidsanchez.pragpooling.domain.model.Neighborhood;
import com.davidsanchez.pragpooling.domain.model.MessageResponse;

import java.util.List;

public interface INeighborhoodServicePort {

    MessageResponse saveNeighborhood(Neighborhood neighborhood);

    List<Neighborhood> getAllNeighborhoods();

    void deleteNeighborhood(Long neighborhoodId);
}