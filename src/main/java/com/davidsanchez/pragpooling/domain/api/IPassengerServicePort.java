package com.davidsanchez.pragpooling.domain.api;

import com.davidsanchez.pragpooling.domain.model.AvailableRide;
import com.davidsanchez.pragpooling.domain.model.CurrentUser;
import com.davidsanchez.pragpooling.domain.model.MessageResponse;
import com.davidsanchez.pragpooling.domain.model.Ride;

import java.util.List;

public interface IPassengerServicePort {
    MessageResponse joinRide(Long rideId, CurrentUser user);

    MessageResponse leaveRoute(CurrentUser user);

    List<Ride> getAvailableRides(CurrentUser user, AvailableRide availableRide);
}