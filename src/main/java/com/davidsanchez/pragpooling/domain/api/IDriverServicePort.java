package com.davidsanchez.pragpooling.domain.api;

import com.davidsanchez.pragpooling.domain.model.CurrentUser;
import com.davidsanchez.pragpooling.domain.model.MessageResponse;
import com.davidsanchez.pragpooling.domain.model.Route;
import com.davidsanchez.pragpooling.domain.model.SaveRoute;

import java.util.List;

public interface IDriverServicePort {
    MessageResponse saveRoute(SaveRoute saveRoute, CurrentUser user);

    List<Route> getAllRoutes(CurrentUser user);
}