package com.davidsanchez.pragpooling.domain.api;

import com.davidsanchez.pragpooling.domain.model.MessageResponse;
import com.davidsanchez.pragpooling.domain.model.Route;

import java.util.List;

public interface IRouteServicePort {
    List<Route> getAllRoutes();

    Route getRouteById(Long routeId);

    MessageResponse deleteRoute(Long routeId);
}