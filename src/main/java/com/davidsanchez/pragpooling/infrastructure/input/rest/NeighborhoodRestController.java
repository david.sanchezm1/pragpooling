package com.davidsanchez.pragpooling.infrastructure.input.rest;

import com.davidsanchez.pragpooling.application.dto.request.NeighborhoodRequestDto;
import com.davidsanchez.pragpooling.application.dto.response.NeighborhoodResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.application.handler.INeighborhoodHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/neighborhood")
@RequiredArgsConstructor
public class NeighborhoodRestController {

    private final INeighborhoodHandler neighborhoodHandler;

    @Operation(summary = "Add a new neighborhood")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Neighborhood created", content = @Content),
            @ApiResponse(responseCode = "409", description = "Neighborhood already exists", content = @Content)
    })
    @PostMapping("/")
    public ResponseEntity<StringResponseDto> saveNeighborhood(@RequestBody NeighborhoodRequestDto neighborhoodRequestDto) {
        return new ResponseEntity<>(neighborhoodHandler.saveNeighborhood(neighborhoodRequestDto), HttpStatus.CREATED);
    }

    @Operation(summary = "Get all neighborhoods")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All neighborhoods returned",
                    content = @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = NeighborhoodResponseDto.class)))),
            @ApiResponse(responseCode = "404", description = "No data found", content = @Content)
    })
    @GetMapping("/")
    public ResponseEntity<List<NeighborhoodResponseDto>> getAllUsers() {
        return ResponseEntity.ok(neighborhoodHandler.getAllNeighborhoods());
    }

    @Operation(summary = "Delete a neighborhood by their id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Neighborhood deleted", content = @Content),
            @ApiResponse(responseCode = "404", description = "Neighborhood not found", content = @Content)
    })
    @DeleteMapping("/{neighborhoodId}")
    public ResponseEntity<Void> deleteNeighborhood(@PathVariable Long neighborhoodId) {
        neighborhoodHandler.deleteNeighborhood(neighborhoodId);
        return ResponseEntity.noContent().build();
    }

}