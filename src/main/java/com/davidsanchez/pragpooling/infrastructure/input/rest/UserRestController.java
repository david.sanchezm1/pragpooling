package com.davidsanchez.pragpooling.infrastructure.input.rest;

import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.application.dto.request.UserLoginRequestDto;
import com.davidsanchez.pragpooling.application.dto.response.UserLoginResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.UserResponseDto;
import com.davidsanchez.pragpooling.application.dto.request.UserSignupRequestDto;
import com.davidsanchez.pragpooling.application.handler.IUserHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class UserRestController {

    private final IUserHandler userHandler;

    @Operation(summary = "Login an user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User created", content = @Content),
            @ApiResponse(responseCode = "409", description = "User already exists", content = @Content)
    })
    @PostMapping("/login")
    public ResponseEntity<UserLoginResponseDto> loginUser(@RequestBody UserLoginRequestDto userLoginRequestDto) {
        return new ResponseEntity<>(userHandler.loginUser(userLoginRequestDto), HttpStatus.CREATED);
    }

    @Operation(summary = "Add a new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User created", content = @Content),
            @ApiResponse(responseCode = "409", description = "User already exists", content = @Content)
    })
    @PostMapping("/signup")
    public ResponseEntity<StringResponseDto> saveUser(@RequestBody UserSignupRequestDto userSignupRequestDto) {
        return new ResponseEntity<>(userHandler.saveUser(userSignupRequestDto), HttpStatus.CREATED);
    }

    @Operation(summary = "Get all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All users returned",
                    content = @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = UserResponseDto.class)))),
            @ApiResponse(responseCode = "404", description = "No data found", content = @Content)
    })
    @SecurityRequirement(name = "bearerAuth")
    @GetMapping("/")
    public ResponseEntity<List<UserResponseDto>> getAllUsers() {
        return ResponseEntity.ok(userHandler.getAllUsers());
    }

    @Operation(summary = "Get a user by its email")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserResponseDto.class))),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    })
    @SecurityRequirement(name = "bearerAuth")
    @GetMapping("/{email}")
    public ResponseEntity<UserResponseDto> getUserByEmail(@Parameter(description = "Email of the user to be returned")
                                                          @PathVariable String email) {
        return ResponseEntity.ok(userHandler.getUserByEmail(email));
    }

    @Operation(summary = "Delete a user by their id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User deleted", content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    })
    @SecurityRequirement(name = "bearerAuth")
    @DeleteMapping("/{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long userId) {
        userHandler.deleteUser(userId);
        return ResponseEntity.noContent().build();
    }
}