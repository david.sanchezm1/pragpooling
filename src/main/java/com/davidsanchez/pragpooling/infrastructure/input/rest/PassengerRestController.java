package com.davidsanchez.pragpooling.infrastructure.input.rest;

import com.davidsanchez.pragpooling.application.dto.request.RideRequestDto;
import com.davidsanchez.pragpooling.application.dto.response.RideResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.application.handler.IPassengerHandler;
import com.davidsanchez.pragpooling.infrastructure.security.CurrentUserRequest;
import com.davidsanchez.pragpooling.infrastructure.security.UserAuthPrincipal;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/passenger")
@RequiredArgsConstructor
public class PassengerRestController {

    private final IPassengerHandler passengerHandler;

    @Operation(summary = "Join a ride")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Joined a ride successfully", content = @Content)
    })
    @PostMapping("/rides/join/{rideId}")
    public ResponseEntity<StringResponseDto> joinRide(@CurrentUserRequest UserAuthPrincipal user,
                                                      @PathVariable Long rideId) {
        return new ResponseEntity<>(passengerHandler.joinRide(rideId, user), HttpStatus.CREATED);
    }

    @Operation(summary = "Leave current ride")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Left a ride successfully", content = @Content),
            @ApiResponse(responseCode = "404", description = "Passenger has no ride", content = @Content)
    })
    @PostMapping("/rides/leave")
    public ResponseEntity<StringResponseDto> leaveRoute(@CurrentUserRequest UserAuthPrincipal user) {
        return new ResponseEntity<>(passengerHandler.leaveRoute(user), HttpStatus.CREATED);
    }

    @Operation(summary = "Get all available rides")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "All rides returned", content = @Content),
            @ApiResponse(responseCode = "404", description = "No data found", content = @Content)
    })
    @PostMapping("/rides")
    public ResponseEntity<List<RideResponseDto>> getAvailableRides(@CurrentUserRequest UserAuthPrincipal user,
                                                                   @RequestBody RideRequestDto rideRequestDto) {
        return ResponseEntity.ok(passengerHandler.getAvailableRides(user, rideRequestDto));
    }
}