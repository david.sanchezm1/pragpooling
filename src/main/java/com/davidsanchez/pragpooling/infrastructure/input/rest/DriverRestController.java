package com.davidsanchez.pragpooling.infrastructure.input.rest;

import com.davidsanchez.pragpooling.application.dto.request.RouteRequestDto;
import com.davidsanchez.pragpooling.application.dto.response.RouteResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.application.handler.IDriverHandler;
import com.davidsanchez.pragpooling.infrastructure.security.CurrentUserRequest;
import com.davidsanchez.pragpooling.infrastructure.security.UserAuthPrincipal;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/driver")
@RequiredArgsConstructor
public class DriverRestController {

    private final IDriverHandler driverHandler;

    @Operation(summary = "Add a new route")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Route created", content = @Content),
            @ApiResponse(responseCode = "409", description = "Route already exists", content = @Content)
    })
    @PostMapping("/routes")
    public ResponseEntity<StringResponseDto> saveRoute(@RequestBody RouteRequestDto routeRequestDto, @CurrentUserRequest UserAuthPrincipal user) {
        return new ResponseEntity<>(driverHandler.saveRoute(routeRequestDto, user), HttpStatus.CREATED);
    }

    @Operation(summary = "Get all driver routes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All routes returned",
                    content = @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = RouteResponseDto.class)))),
            @ApiResponse(responseCode = "404", description = "No data found", content = @Content)
    })
    @GetMapping("/routes")
    public ResponseEntity<List<RouteResponseDto>> getAllRoutes(@CurrentUserRequest UserAuthPrincipal user) {
        return ResponseEntity.ok(driverHandler.getAllRoutes(user));
    }
}