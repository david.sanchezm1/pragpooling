package com.davidsanchez.pragpooling.infrastructure.configuration;

import com.davidsanchez.pragpooling.domain.api.IDriverServicePort;
import com.davidsanchez.pragpooling.domain.api.INeighborhoodServicePort;
import com.davidsanchez.pragpooling.domain.api.IPassengerServicePort;
import com.davidsanchez.pragpooling.domain.api.IRouteServicePort;
import com.davidsanchez.pragpooling.domain.api.IUserServicePort;
import com.davidsanchez.pragpooling.domain.spi.ICognitoPersistencePort;
import com.davidsanchez.pragpooling.domain.spi.INeighborhoodPersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IPathPersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IRidePersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IRoutePersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IUserPersistencePort;
import com.davidsanchez.pragpooling.domain.usecase.DriverUseCase;
import com.davidsanchez.pragpooling.domain.usecase.NeighborhoodUseCase;
import com.davidsanchez.pragpooling.domain.usecase.PassengerUseCase;
import com.davidsanchez.pragpooling.domain.usecase.RouteUseCase;
import com.davidsanchez.pragpooling.domain.usecase.UserUseCase;
import com.davidsanchez.pragpooling.infrastructure.out.http.adapter.CognitoHttpAdapter;
import com.davidsanchez.pragpooling.infrastructure.out.http.mapper.ICognitoLoginModelMapper;
import com.davidsanchez.pragpooling.infrastructure.out.http.mapper.ICognitoSignupModelMapper;
import com.davidsanchez.pragpooling.infrastructure.out.http.mapper.ICognitoTokenModelMapper;
import com.davidsanchez.pragpooling.infrastructure.out.http.service.ICognitoService;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.adapter.NeighborhoodJpaAdapter;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.adapter.PathJpaAdapter;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.adapter.RideJpaAdapter;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.adapter.RouteJpaAdapter;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.adapter.UserJpaAdapter;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper.INeighborhoodEntityMapper;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper.IPathEntityMapper;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper.IRideEntityMapper;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper.IRouteEntityMapper;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper.IUserEntityMapper;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.repository.INeighborhoodRepository;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.repository.IPathRepository;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.repository.IRideRepository;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.repository.IRouteRepository;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.repository.IUserRepository;
import com.davidsanchez.pragpooling.infrastructure.security.JwtUtils;
import com.davidsanchez.pragpooling.infrastructure.security.LoginInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration implements WebMvcConfigurer {

    private final IRideRepository rideRepository;
    private final IRideEntityMapper rideEntityMapper;

    private final IRouteRepository routeRepository;
    private final IRouteEntityMapper routeEntityMapper;

    private final IUserRepository userRepository;
    private final IUserEntityMapper userEntityMapper;

    private final INeighborhoodRepository neighborhoodRepository;
    private final INeighborhoodEntityMapper neighborhoodEntityMapper;

    private final IPathRepository pathRepository;
    private final IPathEntityMapper pathEntityMapper;

    private final ICognitoService cognitoService;
    private final ICognitoSignupModelMapper cognitoModelMapper;
    private final ICognitoLoginModelMapper cognitoLoginModelMapper;
    private final ICognitoTokenModelMapper cognitoTokenModelMapper;


    @Bean
    public ICognitoPersistencePort cognitoPersistencePort() {
        return new CognitoHttpAdapter(cognitoService, cognitoModelMapper, cognitoLoginModelMapper, cognitoTokenModelMapper);
    }

    @Bean
    public IRidePersistencePort ridePersistencePort() {
        return new RideJpaAdapter(rideRepository, rideEntityMapper);
    }

    @Bean
    public IRoutePersistencePort routePersistencePort() {
        return new RouteJpaAdapter(routeRepository, routeEntityMapper);
    }

    @Bean
    public IPathPersistencePort pathPersistencePort() {
        return new PathJpaAdapter(pathRepository, pathEntityMapper);
    }

    @Bean
    public IRouteServicePort routeServicePort() {
        return new RouteUseCase(routePersistencePort());
    }

    @Bean
    public IDriverServicePort driverServicePort() {
        return new DriverUseCase(
                routePersistencePort(),
                userPersistencePort(),
                ridePersistencePort(),
                neighborhoodPersistencePort(),
                pathPersistencePort());
    }

    @Bean
    public IUserPersistencePort userPersistencePort() {
        return new UserJpaAdapter(userRepository, userEntityMapper);
    }

    @Bean
    public IUserServicePort userServicePort() {
        return new UserUseCase(userPersistencePort(), cognitoPersistencePort());
    }

    @Bean
    public INeighborhoodPersistencePort neighborhoodPersistencePort() {
        return new NeighborhoodJpaAdapter(neighborhoodRepository, neighborhoodEntityMapper);
    }

    @Bean
    public INeighborhoodServicePort neighborhoodServicePort() {
        return new NeighborhoodUseCase(neighborhoodPersistencePort());
    }

    @Bean
    public IPassengerServicePort passengerServicePort() {
        return new PassengerUseCase(userPersistencePort(), ridePersistencePort(), neighborhoodPersistencePort());
    }

    @Bean
    public JwtUtils jwtUtils() {
        return new JwtUtils();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor(jwtUtils()))
                .addPathPatterns("/api/v1/**")
                .excludePathPatterns("/api/v1/user/login", "/api/v1/user/signup");
    }

}