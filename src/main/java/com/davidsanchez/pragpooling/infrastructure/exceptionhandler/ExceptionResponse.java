package com.davidsanchez.pragpooling.infrastructure.exceptionhandler;

public enum ExceptionResponse {
    USER_NOT_FOUND("No User was found"),
    USER_ALREADY_EXISTS("There is already a user with that email"),
    NO_DATA_FOUND("No data found for the requested petition"),
    NEIGHBORHOOD_ALREADY_EXISTS("There is already a neighborhood with that name"),
    NEIGHBORHOOD_NOT_FOUND("No Neighborhood found with id %s"),
    NEIGHBORHOOD_DUPLICATED("No duplicated neighborhoods are allowed"),
    ROUTE_NOT_FOUND("No Route found with the given id"),
    COGNITO_ERROR("A problem occurred while trying to connect with cognito"),
    WRONG_CREDENTIALS("Incorrect email or password"),
    MISSING_AUTHORIZATION_HEADER("Missing authorization header with bearer token"),
    ROUTE_ALREADY_JOINED("The user has already joined a route"),
    INVALID_TOKEN("Invalid JWT token"),
    PASSENGER_IS_DRIVER("A driver cannot join his own route"),
    PASSENGER_HAS_NO_ROUTE("Passenger has no route"),
    ENTITY_CANNOT_BE_DELETED("Entity cannot be deleted"),
    PATH_NOT_FOUND("No path found with the given id"),
    RIDE_NOT_FOUND("No ride found with the given id"),
    NO_SEATS_AVAILABLE("There are no more seats available");

    private final String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public String getFormatMessage(RuntimeException runtimeException) {
        return String.format(this.message, runtimeException.getMessage());
    }
}