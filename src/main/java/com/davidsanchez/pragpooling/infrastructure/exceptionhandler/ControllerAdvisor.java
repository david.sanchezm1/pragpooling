package com.davidsanchez.pragpooling.infrastructure.exceptionhandler;

import com.davidsanchez.pragpooling.domain.exception.NoMoreSeatsAvailableException;
import com.davidsanchez.pragpooling.domain.exception.PassengerCantBeTheDriverException;
import com.davidsanchez.pragpooling.domain.exception.PassengerHasNoRideException;
import com.davidsanchez.pragpooling.infrastructure.exception.EntityCannotBeDeletedException;
import com.davidsanchez.pragpooling.infrastructure.exception.InvalidJwtTokenException;
import com.davidsanchez.pragpooling.infrastructure.exception.MissingAuthorizationHeaderException;
import com.davidsanchez.pragpooling.domain.exception.DomainException;
import com.davidsanchez.pragpooling.domain.exception.DuplicateNeighborhoodException;
import com.davidsanchez.pragpooling.infrastructure.exception.RouteAlreadyCreatedException;
import com.davidsanchez.pragpooling.infrastructure.exception.CognitoException;
import com.davidsanchez.pragpooling.infrastructure.exception.NeighborhoodAlreadyExistsException;
import com.davidsanchez.pragpooling.infrastructure.exception.NeighborhoodNotFoundException;
import com.davidsanchez.pragpooling.infrastructure.exception.NoDataFoundException;
import com.davidsanchez.pragpooling.infrastructure.exception.PathNotFoundException;
import com.davidsanchez.pragpooling.infrastructure.exception.RideNotFoundException;
import com.davidsanchez.pragpooling.infrastructure.exception.RouteNotFoundException;
import com.davidsanchez.pragpooling.infrastructure.exception.UserAlreadyExistsException;
import com.davidsanchez.pragpooling.infrastructure.exception.UserNotFoundException;
import com.davidsanchez.pragpooling.infrastructure.exception.WrongCredentialsException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor {

    private static final String MESSAGE = "message";

    @ExceptionHandler(ConstraintViolationException.class)
    ResponseEntity<Map<String, String>> handleConstraintViolation(ConstraintViolationException e) {
        Map<String, String> messages = new HashMap<>();
        for (ConstraintViolation<?> constraintViolation : e.getConstraintViolations()) {
            messages.put(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
        }
        return new ResponseEntity<>(messages, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<Map<String, String>> handleMethodArgumentNotValid(MethodArgumentNotValidException e) {
        Map<String, String> messages = new HashMap<>();

        for (FieldError fieldError : e.getFieldErrors()) {
            messages.put(fieldError.getField(), fieldError.getDefaultMessage());
        }
        return new ResponseEntity<>(messages, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<Map<String, String>> handleNoDataFoundException(
            NoDataFoundException ignoredNoDataFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.NO_DATA_FOUND.getMessage()));
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<Map<String, String>> handleUserAlreadyExistsException(
            UserAlreadyExistsException ignoredUserAlreadyExistsException) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.USER_ALREADY_EXISTS.getMessage()));
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Map<String, String>> handleUserNotFoundException(
            UserNotFoundException ignoredUserNotFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.USER_NOT_FOUND.getMessage()));
    }

    @ExceptionHandler(NeighborhoodAlreadyExistsException.class)
    public ResponseEntity<Map<String, String>> handleNeighborhoodAlreadyExistsException(
            NeighborhoodAlreadyExistsException ignoredNeighborhoodAlreadyExistsException) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.NEIGHBORHOOD_ALREADY_EXISTS.getMessage()));
    }

    @ExceptionHandler(NeighborhoodNotFoundException.class)
    public ResponseEntity<Map<String, String>> handleNeighborhoodNotFoundException(
            NeighborhoodNotFoundException neighborhoodNotFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.NEIGHBORHOOD_NOT_FOUND
                        .getFormatMessage(neighborhoodNotFoundException)));
    }

    @ExceptionHandler(DomainException.class)
    public ResponseEntity<Map<String, String>> handleRequestValidationException(
            DomainException domainException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, domainException.getMessage()));
    }

    @ExceptionHandler(RouteNotFoundException.class)
    public ResponseEntity<Map<String, String>> handleRouteNotFoundException(
            RouteNotFoundException ignoredRouteNotFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.ROUTE_NOT_FOUND.getMessage()));
    }

    @ExceptionHandler(DuplicateNeighborhoodException.class)
    public ResponseEntity<Map<String, String>> handleDuplicateNeighborhoodException(
            DuplicateNeighborhoodException ignoredDuplicateNeighborhoodException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.NEIGHBORHOOD_DUPLICATED.getMessage()));
    }

    @ExceptionHandler(CognitoException.class)
    public ResponseEntity<Map<String, String>> handleCognitoException(
            CognitoException ignoredCognitoException) {
        return ResponseEntity.status(HttpStatus.BAD_GATEWAY)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.COGNITO_ERROR.getMessage()));
    }

    @ExceptionHandler(WrongCredentialsException.class)
    public ResponseEntity<Map<String, String>> handleWrongCredentialsException(
            WrongCredentialsException ignoredWrongCredentialsException) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.WRONG_CREDENTIALS.getMessage()));
    }

    @ExceptionHandler(MissingAuthorizationHeaderException.class)
    public ResponseEntity<Map<String, String>> handleMissingAuthorizationHeaderException(
            MissingAuthorizationHeaderException ignoredMissingAuthorizationHeaderException) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.MISSING_AUTHORIZATION_HEADER.getMessage()));
    }

    @ExceptionHandler(RouteAlreadyCreatedException.class)
    public ResponseEntity<Map<String, String>> handleRouteAlreadyCreatedException(
            RouteAlreadyCreatedException ignoredRouteAlreadyCreatedException) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.ROUTE_ALREADY_JOINED.getMessage()));
    }

    @ExceptionHandler(InvalidJwtTokenException.class)
    public ResponseEntity<Map<String, String>> handleInvalidJwtTokenException(
            InvalidJwtTokenException ignoredInvalidJwtTokenException) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.INVALID_TOKEN.getMessage()));
    }

    @ExceptionHandler(PassengerCantBeTheDriverException.class)
    public ResponseEntity<Map<String, String>> handlePassengerCantBeTheDriverException(
            PassengerCantBeTheDriverException ignoredPassengerCantBeTheDriverException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.PASSENGER_IS_DRIVER.getMessage()));
    }

    @ExceptionHandler(PassengerHasNoRideException.class)
    public ResponseEntity<Map<String, String>> handlePassengerHasNoRideException(
            PassengerHasNoRideException ignoredPassengerHasNoRideException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.PASSENGER_HAS_NO_ROUTE.getMessage()));
    }

    @ExceptionHandler(EntityCannotBeDeletedException.class)
    public ResponseEntity<Map<String, String>> handleEntityCannotBeDeletedException(
            EntityCannotBeDeletedException ignoredEntityCannotBeDeletedException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.ENTITY_CANNOT_BE_DELETED.getMessage()));
    }

    @ExceptionHandler(PathNotFoundException.class)
    public ResponseEntity<Map<String, String>> handlePathNotFoundException(
            PathNotFoundException ignoredPathNotFoundException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.PATH_NOT_FOUND.getMessage()));
    }

    @ExceptionHandler(RideNotFoundException.class)
    public ResponseEntity<Map<String, String>> handleRideNotFoundException(
            RideNotFoundException ignoredRideNotFoundException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.RIDE_NOT_FOUND.getMessage()));
    }

    @ExceptionHandler(NoMoreSeatsAvailableException.class)
    public ResponseEntity<Map<String, String>> handleNoMoreSeatsAvailableException(
            NoMoreSeatsAvailableException ignoredNoMoreSeatsAvailableException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.NO_SEATS_AVAILABLE.getMessage()));
    }


}