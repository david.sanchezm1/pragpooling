package com.davidsanchez.pragpooling.infrastructure.out.http.mapper;

import com.davidsanchez.pragpooling.domain.model.CognitoToken;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoTokenResponse;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface ICognitoTokenModelMapper {

    CognitoToken toModel(CognitoTokenResponse cognitoTokenResponse);
}