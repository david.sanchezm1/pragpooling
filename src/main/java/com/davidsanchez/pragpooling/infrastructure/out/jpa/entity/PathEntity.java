package com.davidsanchez.pragpooling.infrastructure.out.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "path")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PathEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "path_id", nullable = false)
    private Long id;

    @Column()
    private Integer position;

    @Column(name = "neighborhood_id", nullable = false)
    private Long neighborhoodId;

    @Column(name = "route_id", nullable = false)
    private Long routeId;

    @ManyToOne
    @JoinColumn(name = "neighborhood_id", referencedColumnName = "neighborhood_id", nullable = false, insertable=false, updatable=false)
    private NeighborhoodEntity neighborhoodByNeighborhoodId;

    @ManyToOne
    @JoinColumn(name = "route_id", referencedColumnName = "route_id", nullable = false, insertable=false, updatable=false)
    private RouteEntity routeByRouteId;
}
