package com.davidsanchez.pragpooling.infrastructure.out.jpa.adapter;

import com.davidsanchez.pragpooling.domain.model.AvailableRide;
import com.davidsanchez.pragpooling.domain.model.Ride;
import com.davidsanchez.pragpooling.domain.spi.IRidePersistencePort;
import com.davidsanchez.pragpooling.infrastructure.exception.RideNotFoundException;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.RideEntity;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.especification.RideSpecifications;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper.IRideEntityMapper;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.repository.IRideRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class RideJpaAdapter implements IRidePersistencePort {

    private final IRideRepository rideRepository;
    private final IRideEntityMapper rideEntityMapper;

    @Override
    public Ride getRideById(Long rideId) {
        Optional<RideEntity> optionalRide = rideRepository.findById(rideId);
        if (optionalRide.isEmpty()) {
            throw new RideNotFoundException();
        }
        return rideEntityMapper.toRide(optionalRide.get());
    }

    @Override
    public void saveRides(List<Ride> rides) {
        rideRepository.saveAll(rideEntityMapper.toEntityList(rides));
    }

    @Override
    public void decrementAvailableSeats(Long rideId) {
        rideRepository.decrementAvailableSeats(rideId);
    }

    @Override
    public void incrementAvailableSeats(Long rideId) {
        rideRepository.incrementAvailableSeats(rideId);
    }

    @Override
    public List<Ride> getAll() {
        return rideEntityMapper.toRideList(rideRepository.findAll());
    }

    @Override
    public List<Ride> getAllAvailableRides(AvailableRide availableRide) {
        Long originId = availableRide.getNeighborhoodOriginId();
        Long destinationId = availableRide.getNeighborhoodDestinationId();

        Specification<RideEntity> specification = RideSpecifications.hasAvailableRide(availableRide.getDate())
                .and(RideSpecifications.hasPathInOrder(originId, destinationId));

        List<RideEntity> rideEntityList = rideRepository.findAll(specification);

        return rideEntityMapper.toRideList(rideEntityList);
    }

    @Override
    public void decrementStops(Long rideId) {
        rideRepository.decrementStops(rideId);
    }

    @Override
    public void incrementStops(Long rideId) {
        rideRepository.incrementStops(rideId);
    }
}