package com.davidsanchez.pragpooling.infrastructure.out.jpa.repository;

import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.NeighborhoodEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface INeighborhoodRepository extends JpaRepository<NeighborhoodEntity, Long> {
    Optional<NeighborhoodEntity> findByName(String name);
}