package com.davidsanchez.pragpooling.infrastructure.out.jpa.especification;

import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.PathEntity;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.RideEntity;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.RouteEntity;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.sql.Timestamp;

public class RideSpecifications {

    private RideSpecifications() {

    }

    private static final Integer INDEX_FIRST_PATH = 1;
    private static final Integer MIN_SEATS_AVAILABLE = 1;
    private static final String POSITION = "position";

    public static Specification<RideEntity> hasAvailableRide(Timestamp date) {
        return (root, query, criteriaBuilder) -> {
            query.orderBy(criteriaBuilder.asc(root.get("departureTime")));
            return criteriaBuilder.and(
                    criteriaBuilder.greaterThanOrEqualTo(root.get("departureTime"), date),
                    criteriaBuilder.greaterThanOrEqualTo(root.get("availableSeats"), MIN_SEATS_AVAILABLE)
            );
        };
    }

    public static Specification<RideEntity> hasPathInOrder(Long originId, Long destinationId) {
        return (root, query, criteriaBuilder) -> {

            Join<RideEntity, RouteEntity> routeRideOrigin = root.join("routeByRouteId");
            Join<RouteEntity, PathEntity> pathRouteOrigin = routeRideOrigin.join("pathsByRouteId");

            Join<RideEntity, RouteEntity> routeRideDestination = root.join("routeByRouteId");
            Join<RouteEntity, PathEntity> pathRouteDestination = routeRideDestination.join("pathsByRouteId");


            return criteriaBuilder.and(
                    // destination pos > origin pos
                    criteriaBuilder.greaterThan(pathRouteDestination.get(POSITION), pathRouteOrigin.get(POSITION)),

                    // include neighborhood origin id
                    criteriaBuilder.equal(pathRouteOrigin.get("neighborhoodId"), originId),

                    // include neighborhood destination id
                    criteriaBuilder.equal(pathRouteDestination.get("neighborhoodId"), destinationId),

                    // origin pos is not the last one
                    criteriaBuilder.lessThan(pathRouteOrigin.get(POSITION), routeRideOrigin.get("totalPaths")),

                    // destination pos is not the first one
                    criteriaBuilder.greaterThan(pathRouteDestination.get(POSITION), INDEX_FIRST_PATH)
            );
        };
    }
}
