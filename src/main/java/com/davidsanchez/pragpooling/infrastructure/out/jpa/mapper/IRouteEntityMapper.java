package com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper;

import com.davidsanchez.pragpooling.domain.model.Route;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.RouteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IRouteEntityMapper {

    RouteEntity toEntity(Route route);

    Route toRoute(RouteEntity routeEntity);

    List<Route> toRouteList(List<RouteEntity> routeEntityList);
}