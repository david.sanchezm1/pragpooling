package com.davidsanchez.pragpooling.infrastructure.out.http.model;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CognitoLoginUserBodyResponse {
    @SerializedName("Errors")
    private List<String> errors;

    @SerializedName("Data")
    private CognitoTokenResponse tokenResponse;

    @SerializedName("Message")
    private String message;
}
