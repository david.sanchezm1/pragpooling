package com.davidsanchez.pragpooling.infrastructure.out.http.service;

import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoLoginUserRequest;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoSignupUserRequest;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoTokenResponse;

public interface ICognitoService {

    String signup(CognitoSignupUserRequest signupUserRequest);

    CognitoTokenResponse login(CognitoLoginUserRequest loginUserRequest);
}
