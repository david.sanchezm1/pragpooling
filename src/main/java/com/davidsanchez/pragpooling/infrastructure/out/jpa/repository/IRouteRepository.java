package com.davidsanchez.pragpooling.infrastructure.out.jpa.repository;

import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.RouteEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IRouteRepository extends JpaRepository<RouteEntity, Long> {
    List<RouteEntity> findAllByDriverId(Long driverId);
}