package com.davidsanchez.pragpooling.infrastructure.out.http.mapper;

import com.davidsanchez.pragpooling.domain.model.User;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoSignupUserRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface ICognitoSignupModelMapper {

    @Mapping(source = "firstName", target = "name")
    @Mapping(source = "lastName", target = "familyName")
    CognitoSignupUserRequest toModel(User user);
}