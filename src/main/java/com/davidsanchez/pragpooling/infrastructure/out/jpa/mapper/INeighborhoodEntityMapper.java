package com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper;

import com.davidsanchez.pragpooling.domain.model.Neighborhood;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.NeighborhoodEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface INeighborhoodEntityMapper {

    NeighborhoodEntity toEntity(Neighborhood user);

    Neighborhood toNeighborhood(NeighborhoodEntity userEntity);

    List<Neighborhood> toNeighborhoodList(List<NeighborhoodEntity> userEntityList);
}