package com.davidsanchez.pragpooling.infrastructure.out.http.model;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CognitoTokenResponse {
    @SerializedName("AccessToken")
    private String accessToken;

    @SerializedName("IdToken")
    private String idToken;

    @SerializedName("RefreshToken")
    private String refreshToken;
}
