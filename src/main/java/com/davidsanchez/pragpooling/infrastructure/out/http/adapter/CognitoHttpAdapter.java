package com.davidsanchez.pragpooling.infrastructure.out.http.adapter;

import com.davidsanchez.pragpooling.domain.model.CognitoToken;
import com.davidsanchez.pragpooling.domain.model.User;
import com.davidsanchez.pragpooling.domain.spi.ICognitoPersistencePort;
import com.davidsanchez.pragpooling.infrastructure.out.http.mapper.ICognitoLoginModelMapper;
import com.davidsanchez.pragpooling.infrastructure.out.http.mapper.ICognitoSignupModelMapper;
import com.davidsanchez.pragpooling.infrastructure.out.http.mapper.ICognitoTokenModelMapper;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoTokenResponse;
import com.davidsanchez.pragpooling.infrastructure.out.http.service.ICognitoService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CognitoHttpAdapter implements ICognitoPersistencePort {

    private final ICognitoService cognitoService;
    private final ICognitoSignupModelMapper cognitoSignupModelMapper;
    private final ICognitoLoginModelMapper cognitoLoginModelMapper;
    private final ICognitoTokenModelMapper cognitoTokenModelMapper;

    @Override
    public String signupUser(User user) {
        return cognitoService.signup(cognitoSignupModelMapper.toModel(user));
    }

    @Override
    public CognitoToken loginUser(User user) {
        CognitoTokenResponse cognitoTokenResponse = cognitoService.login(cognitoLoginModelMapper.toModel(user));
        return cognitoTokenModelMapper.toModel(cognitoTokenResponse);
    }
}
