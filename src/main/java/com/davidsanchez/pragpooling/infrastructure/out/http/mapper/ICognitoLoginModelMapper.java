package com.davidsanchez.pragpooling.infrastructure.out.http.mapper;

import com.davidsanchez.pragpooling.domain.model.User;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoLoginUserRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface ICognitoLoginModelMapper {

    @Mapping(source = "username", target = "name")
    CognitoLoginUserRequest toModel(User user);
}