package com.davidsanchez.pragpooling.infrastructure.out.jpa.adapter;

import com.davidsanchez.pragpooling.domain.model.Neighborhood;
import com.davidsanchez.pragpooling.domain.spi.INeighborhoodPersistencePort;
import com.davidsanchez.pragpooling.infrastructure.exception.EntityCannotBeDeletedException;
import com.davidsanchez.pragpooling.infrastructure.exception.NeighborhoodAlreadyExistsException;
import com.davidsanchez.pragpooling.infrastructure.exception.NeighborhoodNotFoundException;
import com.davidsanchez.pragpooling.infrastructure.exception.NoDataFoundException;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.NeighborhoodEntity;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper.INeighborhoodEntityMapper;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.repository.INeighborhoodRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class NeighborhoodJpaAdapter implements INeighborhoodPersistencePort {

    private final INeighborhoodRepository neighborhoodRepository;
    private final INeighborhoodEntityMapper neighborhoodEntityMapper;

    @Override
    public Neighborhood saveNeighborhood(Neighborhood neighborhood) {
        if (neighborhoodRepository.findByName(neighborhood.getName()).isPresent()) {
            throw new NeighborhoodAlreadyExistsException();
        }
        NeighborhoodEntity neighborhoodEntity = neighborhoodRepository.save(neighborhoodEntityMapper.toEntity(neighborhood));
        return neighborhoodEntityMapper.toNeighborhood(neighborhoodEntity);
    }

    @Override
    public List<Neighborhood> getAllNeighborhoods() {
        List<NeighborhoodEntity> neighborhoodEntityList = neighborhoodRepository.findAll();
        if (neighborhoodEntityList.isEmpty()) {
            throw new NoDataFoundException();
        }
        return neighborhoodEntityMapper.toNeighborhoodList(neighborhoodRepository.findAll());
    }

    @Override
    public void deleteNeighborhood(Long neighborhoodId) {
        Optional<NeighborhoodEntity> optionalNeighborhood = neighborhoodRepository.findById(neighborhoodId);
        if (optionalNeighborhood.isEmpty()) {
            throw new NeighborhoodNotFoundException(neighborhoodId);
        }

        NeighborhoodEntity neighborhoodEntity = optionalNeighborhood.get();

        if (!neighborhoodEntity.getPathsByNeighborhoodId().isEmpty()) {
            throw new EntityCannotBeDeletedException();
        }

        neighborhoodRepository.deleteById(neighborhoodId);
    }

    @Override
    public void validateIds(List<Long> neighborhoodIds) {
        for (Long neighborhoodId : neighborhoodIds) {
            Optional<NeighborhoodEntity> userEntity = neighborhoodRepository.findById(neighborhoodId);
            if (userEntity.isEmpty()) {
                throw new NeighborhoodNotFoundException(neighborhoodId);
            }
        }
    }

    @Override
    public Neighborhood findNeighborhoodById(Long neighborhoodId) {
        Optional<NeighborhoodEntity> optionalNeighborhood = neighborhoodRepository.findById(neighborhoodId);
        if (optionalNeighborhood.isEmpty()) {
            throw new NeighborhoodNotFoundException(neighborhoodId);
        }

        return neighborhoodEntityMapper.toNeighborhood(optionalNeighborhood.get());
    }


}