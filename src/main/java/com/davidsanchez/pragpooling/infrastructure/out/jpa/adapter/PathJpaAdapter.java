package com.davidsanchez.pragpooling.infrastructure.out.jpa.adapter;

import com.davidsanchez.pragpooling.domain.model.Path;
import com.davidsanchez.pragpooling.domain.spi.IPathPersistencePort;
import com.davidsanchez.pragpooling.infrastructure.exception.NoDataFoundException;
import com.davidsanchez.pragpooling.infrastructure.exception.PathNotFoundException;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.PathEntity;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper.IPathEntityMapper;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.repository.IPathRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class PathJpaAdapter implements IPathPersistencePort {

    private final IPathRepository pathRepository;
    private final IPathEntityMapper pathEntityMapper;

    @Override
    public Path savePath(Path path) {
        PathEntity pathEntity = pathRepository.save(pathEntityMapper.toEntity(path));
        return pathEntityMapper.toPath(pathEntity);
    }

    @Override
    public List<Path> savePaths(List<Path> pathList) {
        List<PathEntity> pathEntityList = pathRepository.saveAll(pathEntityMapper.toEntityList(pathList));
        return pathEntityMapper.toPathList(pathEntityList);
    }

    @Override
    public List<Path> getAllPaths() {
        List<PathEntity> pathEntityList = pathRepository.findAll();
        if (pathEntityList.isEmpty()) {
            throw new NoDataFoundException();
        }
        return pathEntityMapper.toPathList(pathRepository.findAll());
    }

    @Override
    public Path getPathById(Long pathId) {
        Optional<PathEntity> optionalPath = pathRepository.findById(pathId);
        if (optionalPath.isEmpty()) {
            throw new PathNotFoundException();
        }
        return pathEntityMapper.toPath(optionalPath.get());
    }

}