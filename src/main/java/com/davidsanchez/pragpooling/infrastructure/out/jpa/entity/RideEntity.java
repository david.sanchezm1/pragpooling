package com.davidsanchez.pragpooling.infrastructure.out.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "ride")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RideEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "ride_id", nullable = false)
    private Long id;

    @Column(name = "departure_time")
    private Timestamp departureTime;

    @Column(name = "available_seats")
    private Integer availableSeats;

    @Column(name = "meeting_point", length = 100)
    private String meetingPoint;

    @Column()
    private Integer stops;

    @Column(name = "route_id", nullable = false)
    private Long routeId;

    @ManyToOne
    @JoinColumn(name = "route_id", referencedColumnName = "route_id", nullable = false, insertable=false, updatable=false)
    private RouteEntity routeByRouteId;

    @OneToMany(mappedBy = "rideByRideId")
    private Collection<UserEntity> usersByRideId;
}
