package com.davidsanchez.pragpooling.infrastructure.out.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "route")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RouteEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "route_id", nullable = false)
    private Long id;

    @Column()
    private Integer totalPaths;

    @Column(name = "driver_id", nullable = false)
    private Long driverId;

    @OneToMany(mappedBy = "routeByRouteId", cascade = CascadeType.REMOVE)
    private Collection<PathEntity> pathsByRouteId;

    @OneToMany(mappedBy = "routeByRouteId", cascade = CascadeType.REMOVE)
    private Collection<RideEntity> ridesByRouteId;
}
