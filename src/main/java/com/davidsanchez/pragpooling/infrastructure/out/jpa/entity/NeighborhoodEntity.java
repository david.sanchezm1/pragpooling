package com.davidsanchez.pragpooling.infrastructure.out.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "neighborhood")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class NeighborhoodEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "neighborhood_id", nullable = false)
    private Long id;

    @Column(length = 50)
    private String name;

    @OneToMany(mappedBy = "neighborhoodByNeighborhoodId")
    private Collection<PathEntity> pathsByNeighborhoodId;
}
