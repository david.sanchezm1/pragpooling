package com.davidsanchez.pragpooling.infrastructure.out.jpa.adapter;

import com.davidsanchez.pragpooling.infrastructure.exception.RouteAlreadyCreatedException;
import com.davidsanchez.pragpooling.domain.model.User;
import com.davidsanchez.pragpooling.domain.spi.IUserPersistencePort;
import com.davidsanchez.pragpooling.infrastructure.exception.NoDataFoundException;
import com.davidsanchez.pragpooling.infrastructure.exception.UserAlreadyExistsException;
import com.davidsanchez.pragpooling.infrastructure.exception.UserNotFoundException;
import com.davidsanchez.pragpooling.infrastructure.exception.WrongCredentialsException;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.UserEntity;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper.IUserEntityMapper;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.repository.IUserRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class UserJpaAdapter implements IUserPersistencePort {

    private final IUserRepository userRepository;
    private final IUserEntityMapper userEntityMapper;

    @Override
    public User joinRide(User user, Long rideId) {
        Optional<UserEntity> userEntity = userRepository.findById(user.getId());
        if (userEntity.isEmpty()) {
            throw new UserNotFoundException();
        }
        UserEntity userToUpdate = userEntity.get();
        if (userToUpdate.getRideByRideId() != null) {
            throw new RouteAlreadyCreatedException();
        }

        userToUpdate.setRideId(rideId);
        return userEntityMapper.toUser(userRepository.save(userToUpdate));
    }

    @Override
    public User leaveRide(User user) {
        user.setRideId(null);
        UserEntity userEntity = userRepository.save(userEntityMapper.toEntity(user));
        return userEntityMapper.toUser(userEntity);
    }

    @Override
    public User saveUser(User user) {
        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            throw new UserAlreadyExistsException();
        }
        UserEntity userEntity = userRepository.save(userEntityMapper.toEntity(user));
        return userEntityMapper.toUser(userEntity);
    }

    @Override
    public List<User> getAllUsers() {
        List<UserEntity> userEntityList = userRepository.findAll();
        if (userEntityList.isEmpty()) {
            throw new NoDataFoundException();
        }
        return userEntityMapper.toUserList(userEntityList);
    }

    @Override
    public User getUserByEmail(String email) {
        Optional<UserEntity> user = userRepository.findByEmail(email);
        if (user.isEmpty()) {
            throw new UserNotFoundException();
        }
        return userEntityMapper.toUser(user.get());
    }

    @Override
    public User getUserByUsername(String username) {
        Optional<UserEntity> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new UserNotFoundException();
        }
        return userEntityMapper.toUser(user.get());
    }

    @Override
    public User getUserById(Long userId) {
        Optional<UserEntity> user = userRepository.findById(userId);
        if (user.isEmpty()) {
            throw new UserNotFoundException();
        }
        return userEntityMapper.toUser(user.get());
    }

    @Override
    public void deleteUser(Long userId) {
        Optional<UserEntity> userEntity = userRepository.findById(userId);
        if (userEntity.isEmpty()) {
            throw new UserNotFoundException();
        }
        userRepository.deleteById(userId);
    }

    @Override
    public void validateEmailUniqueness(String email) {
        if (userRepository.findByEmail(email).isPresent()) {
            throw new UserAlreadyExistsException();
        }
    }

    @Override
    public User getUserByEmailAndPassword(User user) {
        Optional<UserEntity> userEntity = userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword());
        if (userEntity.isEmpty()) {
            throw new WrongCredentialsException();
        }
        return userEntityMapper.toUser(userEntity.get());
    }
}