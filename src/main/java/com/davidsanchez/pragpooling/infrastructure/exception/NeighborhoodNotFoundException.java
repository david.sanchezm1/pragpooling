package com.davidsanchez.pragpooling.infrastructure.exception;

public class NeighborhoodNotFoundException extends RuntimeException {
    public NeighborhoodNotFoundException(Long id) {
        super(Long.toString(id));
    }
}
