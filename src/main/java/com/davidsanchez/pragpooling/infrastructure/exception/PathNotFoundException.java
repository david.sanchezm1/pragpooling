package com.davidsanchez.pragpooling.infrastructure.exception;

public class PathNotFoundException extends RuntimeException {
    public PathNotFoundException() {
        super();
    }
}
