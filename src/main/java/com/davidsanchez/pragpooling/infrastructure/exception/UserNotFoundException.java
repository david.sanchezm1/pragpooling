package com.davidsanchez.pragpooling.infrastructure.exception;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException() {
        super();
    }
}
