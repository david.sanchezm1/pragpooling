package com.davidsanchez.pragpooling.infrastructure.exception;

public class InvalidJwtTokenException extends RuntimeException {
    public InvalidJwtTokenException() {
        super();
    }
}
