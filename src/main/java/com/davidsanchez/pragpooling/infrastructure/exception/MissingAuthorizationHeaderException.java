package com.davidsanchez.pragpooling.infrastructure.exception;

public class MissingAuthorizationHeaderException extends RuntimeException {
    public MissingAuthorizationHeaderException() {
        super();
    }
}
