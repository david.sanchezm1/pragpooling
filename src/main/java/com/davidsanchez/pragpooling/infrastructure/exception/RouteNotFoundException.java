package com.davidsanchez.pragpooling.infrastructure.exception;

public class RouteNotFoundException extends RuntimeException {
    public RouteNotFoundException() {
        super();
    }
}
