package com.davidsanchez.pragpooling.infrastructure.exception;

public class NeighborhoodAlreadyExistsException extends RuntimeException{
    public NeighborhoodAlreadyExistsException() {
        super();
    }
}
