package com.davidsanchez.pragpooling.infrastructure.security;

import com.davidsanchez.pragpooling.infrastructure.exception.InvalidJwtTokenException;
import com.google.gson.Gson;
import com.nimbusds.jwt.SignedJWT;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;

public class JwtUtils {

    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public String parseJWT(String accessToken) {
        try {
            var decodedJWT = SignedJWT.parse(accessToken);
            return decodedJWT.getPayload().toString();
        } catch (ParseException e) {
            throw new InvalidJwtTokenException();
        }
    }

    public UserData parsePayload(String payload) {
        Gson gson = new Gson();
        UserData userData = gson.fromJson(
                payload,
                UserData.class
        );

        if (StringUtils.isEmpty(userData.getUsername())) {
            throw new InvalidJwtTokenException();
        }

        return userData;
    }
}
