package com.davidsanchez.pragpooling.domain.usecase;

import com.davidsanchez.pragpooling.domain.model.CurrentUser;
import com.davidsanchez.pragpooling.domain.model.Route;
import com.davidsanchez.pragpooling.domain.model.SaveRoute;
import com.davidsanchez.pragpooling.domain.model.User;
import com.davidsanchez.pragpooling.domain.spi.INeighborhoodPersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IPathPersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IRidePersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IRoutePersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IUserPersistencePort;
import com.davidsanchez.pragpooling.factory.RouteFactoryDataTest;
import com.davidsanchez.pragpooling.factory.SaveRouteFactoryDataTest;
import com.davidsanchez.pragpooling.factory.UserFactoryDataTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class DriverUseCaseTest {

    @InjectMocks
    DriverUseCase driverUseCase;

    @Mock
    IRoutePersistencePort routePersistencePort;

    @Mock
    IUserPersistencePort userPersistencePort;

    @Mock
    IRidePersistencePort ridePersistencePort;

    @Mock
    INeighborhoodPersistencePort neighborhoodPersistencePort;

    @Mock
    IPathPersistencePort pathPersistencePort;


    @Test
    void should_save_route_given_valid_request() {
        // Given
        SaveRoute saveRoute = SaveRouteFactoryDataTest.getSaveRoute();
        CurrentUser currentUser = UserFactoryDataTest.getCurrentUser();
        User driver = UserFactoryDataTest.getUser();
        Route route = RouteFactoryDataTest.getRoute();

        // When
        when(userPersistencePort.getUserByUsername(currentUser.getUsername())).thenReturn(driver);
        when(routePersistencePort.saveRoute(any(Route.class))).thenReturn(route);
        driverUseCase.saveRoute(saveRoute, currentUser);

        // Then
        verify(neighborhoodPersistencePort).validateIds(saveRoute.getNeighborhoods());
        verify(ridePersistencePort).saveRides(anyList());
        verify(pathPersistencePort).savePaths(anyList());
    }

    @Test
    void should_get_all_routes_given_valid_request() {
        // Given
        CurrentUser currentUser = UserFactoryDataTest.getCurrentUser();
        User driver = UserFactoryDataTest.getUser();

        // When
        when(userPersistencePort.getUserByUsername(currentUser.getUsername())).thenReturn(driver);
        driverUseCase.getAllRoutes(currentUser);

        // Then
        verify(routePersistencePort).getAllRoutesByDriverId(anyLong());
    }
}