package com.davidsanchez.pragpooling.domain.usecase;

import com.davidsanchez.pragpooling.domain.model.User;
import com.davidsanchez.pragpooling.domain.spi.ICognitoPersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IUserPersistencePort;
import com.davidsanchez.pragpooling.factory.UserFactoryDataTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class UserUseCaseTest {

    @InjectMocks
    UserUseCase userUseCase;

    @Mock
    IUserPersistencePort userPersistencePort;

    @Mock
    ICognitoPersistencePort cognitoPersistencePort;

    @Test
    void should_login_user_given_valid_email_and_password() {
        // Given
        User user = UserFactoryDataTest.getUser();

        // When
        when(userPersistencePort.getUserByEmailAndPassword(user)).thenReturn(user);
        userUseCase.loginUser(user);

        // Then
        verify(cognitoPersistencePort).loginUser(user);
    }

    @Test
    void when_saveUser_expect_persistencePort_saveUser() {
        // Given
        User user = UserFactoryDataTest.getUser();

        // When
        when(cognitoPersistencePort.signupUser(any())).thenReturn(user.getUsername());
        when(userPersistencePort.saveUser(any())).thenReturn(user);
        userUseCase.saveUser(user);

        // Then
        verify(userPersistencePort).saveUser(user);
    }

    @Test
    void when_getAllUsers_expect_persistencePort_getAllUsers() {
        // When
        userUseCase.getAllUsers();

        // Then
        verify(userPersistencePort).getAllUsers();
    }

    @Test
    void when_getUserByEmail_expect_persistencePort_getUserByEmail() {
        // When
        userUseCase.getUserByEmail(anyString());

        // Then
        verify(userPersistencePort).getUserByEmail(anyString());
    }

    @Test
    void when_deleteUser_expect_persistencePort_deleteUser() {
        // When
        userUseCase.deleteUser(anyLong());

        // Then
        verify(userPersistencePort).deleteUser(anyLong());
    }
}