package com.davidsanchez.pragpooling.domain.usecase;

import com.davidsanchez.pragpooling.domain.spi.IRoutePersistencePort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
class RouteUseCaseTest {

    @InjectMocks
    RouteUseCase routeUseCase;

    @Mock
    IRoutePersistencePort routePersistencePort;

    @Test
    void when_getAllRoutes_expect_persistencePort_getAllRoutes() {
        // When
        routeUseCase.getAllRoutes();

        // Then
        verify(routePersistencePort).getAllRoutes();
    }

    @Test
    void when_getRouteById_expect_persistencePort_getRouteById() {
        // When
        routeUseCase.getRouteById(anyLong());

        // Then
        verify(routePersistencePort).getRouteById(anyLong());
    }

    @Test
    void when_deleteRoute_expect_persistencePort_deleteRoute() {
        // When
        routeUseCase.deleteRoute(anyLong());

        // Then
        verify(routePersistencePort).deleteRoute(anyLong());
    }
}