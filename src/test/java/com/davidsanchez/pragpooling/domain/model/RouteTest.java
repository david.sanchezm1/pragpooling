package com.davidsanchez.pragpooling.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class RouteTest {

    @Test
    void setId() {
        Route route = new Route();
        route.setId(1L);
        assertEquals(1L, route.getId());
    }

    @Test
    void setDriverId() {
        Route route = new Route();
        route.setDriverId(1L);
        assertEquals(1L, route.getDriverId());
    }
}