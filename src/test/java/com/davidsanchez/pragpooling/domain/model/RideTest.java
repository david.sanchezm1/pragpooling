package com.davidsanchez.pragpooling.domain.model;

import com.davidsanchez.pragpooling.util.DateUtil;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertEquals;


class RideTest {

    @Test
    void mustSetAndGetId() {
        Ride ride = new Ride();
        ride.setId(1L);
        assertEquals(1L, ride.getId());
    }

    @Test
    void mustSetAndGetDepartureTime() {
        Ride ride = new Ride();
        Timestamp timestamp = DateUtil.createTimestampTest();
        ride.setDepartureTime(timestamp);
        assertEquals(timestamp, ride.getDepartureTime());
    }
}