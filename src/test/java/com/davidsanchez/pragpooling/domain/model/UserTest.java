package com.davidsanchez.pragpooling.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserTest {

    @Test
    void mustGetAndSetAndSetId() {
        User user = new User();
        user.setId(1L);
        assertEquals(1L, user.getId());
    }

    @Test
    void mustGetAndSetFirstName() {
        User user = new User();
        user.setFirstName("David");
        assertEquals("David", user.getFirstName());
    }

    @Test
    void mustGetAndSetLastName() {
        User user = new User();
        user.setLastName("Sanchez");
        assertEquals("Sanchez", user.getLastName());
    }

    @Test
    void mustGetAndSetEmail() {
        User user = new User();
        user.setEmail("david@gmail.com");
        assertEquals("david@gmail.com", user.getEmail());
    }

}