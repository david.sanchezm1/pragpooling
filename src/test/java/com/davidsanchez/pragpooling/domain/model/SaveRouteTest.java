package com.davidsanchez.pragpooling.domain.model;

import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SaveRouteTest {

    @Test
    void mustSetAndGetSeats() {
        SaveRoute saveRoute = new SaveRoute();
        saveRoute.setSeats(1);
        assertEquals(1, saveRoute.getSeats());
    }

    @Test
    void mustSetAndGetRides() {
        SaveRoute saveRoute = new SaveRoute();
        List<Timestamp> timestamps = new ArrayList<>();
        saveRoute.setRides(timestamps);
        assertEquals(timestamps, saveRoute.getRides());
    }

    @Test
    void mustSetAndGetNeighborhoods() {
        SaveRoute saveRoute = new SaveRoute();
        List<Long> list = new ArrayList<>();
        saveRoute.setNeighborhoods(list);
        assertEquals(list, saveRoute.getNeighborhoods());
    }

    @Test
    void mustSetAndGetDescription() {
        SaveRoute saveRoute = new SaveRoute();
        saveRoute.setDescription("Description");
        assertEquals("Description", saveRoute.getDescription());
    }

    @Test
    void mustSetAndGetMeetingPoint() {
        SaveRoute saveRoute = new SaveRoute();
        String meetingPoint = "meeting point here";
        saveRoute.setMeetingPoint(meetingPoint);
        assertEquals(meetingPoint, saveRoute.getMeetingPoint());
    }
}