package com.davidsanchez.pragpooling.domain.validation;

import com.davidsanchez.pragpooling.domain.exception.DomainException;
import com.davidsanchez.pragpooling.domain.exception.DuplicateNeighborhoodException;
import com.davidsanchez.pragpooling.domain.model.SaveRoute;
import com.davidsanchez.pragpooling.factory.SaveRouteFactoryDataTest;
import org.junit.jupiter.api.Test;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;

class DriverValidatorTest {

    @Test
    void when_duplicateNeighborhood_expect_DuplicateNeighborhoodException() {
        // Given
        SaveRoute saveRoute = SaveRouteFactoryDataTest.getSaveRoute();
        List<Long> duplicateNeighborhoods = List.of(1L, 2L, 2L, 3L, 3L);
        saveRoute.setNeighborhoods(duplicateNeighborhoods);

        // When
        Throwable throwable = catchThrowable(() -> DriverValidator.validateSaveRoute(saveRoute));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DuplicateNeighborhoodException.class);
    }

    @Test
    void when_notDuplicateNeighborhood_shouldNot_throwAnything() {
        // Given
        SaveRoute saveRoute = SaveRouteFactoryDataTest.getSaveRoute();
        List<Long> validNeighborhoods = List.of(1L, 2L);
        saveRoute.setNeighborhoods(validNeighborhoods);

        // When
        Throwable throwable = catchThrowable(() -> DriverValidator.validateSaveRoute(saveRoute));

        // Then
        assertThat(throwable).isNull();
    }

    @Test
    void when_seatsIsBetween1And4_shouldNot_throwAnything() {
        // Given
        SaveRoute saveRoute = SaveRouteFactoryDataTest.getSaveRoute();
        saveRoute.setSeats(3);

        // When
        Throwable throwable = catchThrowable(() -> DriverValidator.validateSaveRoute(saveRoute));

        // Then
        assertThat(throwable).isNull();
    }

    @Test
    void when_seatsGreaterThan4_expect_DomainException() {
        // Given
        SaveRoute saveRoute = SaveRouteFactoryDataTest.getSaveRoute();
        saveRoute.setSeats(5);

        // When
        Throwable throwable = catchThrowable(() -> DriverValidator.validateSaveRoute(saveRoute));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    @Test
    void when_seatsLessThan1_expect_DomainException() {
        // Given
        SaveRoute saveRoute = SaveRouteFactoryDataTest.getSaveRoute();
        saveRoute.setSeats(0);

        // When
        Throwable throwable = catchThrowable(() -> DriverValidator.validateSaveRoute(saveRoute));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    @Test
    void when_rideSchedulesLessThan1_expect_DomainException() {
        // Given
        SaveRoute saveRoute = SaveRouteFactoryDataTest.getSaveRoute();
        List<Timestamp> rides = new ArrayList<>();
        saveRoute.setRides(rides);

        // When
        Throwable throwable = catchThrowable(() -> DriverValidator.validateSaveRoute(saveRoute));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    @Test
    void when_neighborhoodsGreaterThan1_shouldNot_throwAnything() {
        // Given
        SaveRoute saveRoute = SaveRouteFactoryDataTest.getSaveRoute();
        saveRoute.setNeighborhoods(List.of(1L, 2L));

        // When
        Throwable throwable = catchThrowable(() -> DriverValidator.validateSaveRoute(saveRoute));

        // Then
        assertThat(throwable).isNull();
    }

    @Test
    void when_neighborhoodsLessThan2_expect_DomainException() {
        // Given
        SaveRoute saveRoute = SaveRouteFactoryDataTest.getSaveRoute();
        saveRoute.setNeighborhoods(List.of(1L));

        // When
        Throwable throwable = catchThrowable(() -> DriverValidator.validateSaveRoute(saveRoute));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }
}