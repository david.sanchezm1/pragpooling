package com.davidsanchez.pragpooling.domain.validation;

import com.davidsanchez.pragpooling.domain.exception.DomainException;
import com.davidsanchez.pragpooling.domain.model.User;
import com.davidsanchez.pragpooling.factory.UserFactoryDataTest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class UserValidatorTest {

    @Test
    void when_emailIsInvalid_expect_DomainException() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setEmail("invalid@email");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateSaveUser(user));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    @Test
    void when_emailIsValid_shouldNot_throwAnything() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setEmail("valid@email.com");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateSaveUser(user));

        // Then
        assertThat(throwable).isNull();
    }

    @Test
    void when_FirstNameIsBlank_expect_DomainException() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setFirstName("");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateSaveUser(user));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    @Test
    void when_FirstNameIsNotBlank_shouldNot_throwAnything() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setFirstName("Tara");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateSaveUser(user));

        // Then
        assertThat(throwable).isNull();
    }

    @Test
    void when_LastNameIsBlank_expect_DomainException() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setLastName("");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateSaveUser(user));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    @Test
    void when_LastNameIsNotBlank_shouldNot_throwAnything() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setLastName("Stokes");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateSaveUser(user));

        // Then
        assertThat(throwable).isNull();
    }

    @Test
    void when_AddressIsBlank_expect_DomainException() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setAddress("");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateSaveUser(user));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    @Test
    void when_AddressIsNotBlank_shouldNot_throwAnything() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setAddress("1825 Muir Junction");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateSaveUser(user));

        // Then
        assertThat(throwable).isNull();
    }

    @Test
    void when_PhoneNumberIsBlank_expect_DomainException() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setPhoneNumber("");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateSaveUser(user));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    @Test
    void when_PhoneNumberIsNotBlank_shouldNot_throwAnything() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setPhoneNumber("708-553-7102");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateSaveUser(user));

        // Then
        assertThat(throwable).isNull();
    }

    @Test
    void when_PasswordIsBlank_expect_DomainException() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setPassword("");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateSaveUser(user));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    @Test
    void when_PasswordIsValid_shouldNot_throwAnything() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setPassword("HelloWorld1234*");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateSaveUser(user));

        // Then
        assertThat(throwable).isNull();
    }

    @Test
    void should_throw_exception_given_invalid_email_when_login() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setEmail("test@test");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateLoginUser(user));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    @Test
    void should_throw_exception_given_blank_password_when_login() {
        // Given
        User user = UserFactoryDataTest.getUser();
        user.setPassword("");

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateLoginUser(user));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    @Test
    void should_validate_user_given_valid_login_user() {
        // Given
        User user = UserFactoryDataTest.getUser();

        // When
        Throwable throwable = catchThrowable(() -> UserValidator.validateLoginUser(user));

        // Then
        assertThat(throwable).isNull();
    }
}