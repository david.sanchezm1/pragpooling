package com.davidsanchez.pragpooling.domain.validation;

import com.davidsanchez.pragpooling.domain.exception.DomainException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class ValidationUtilsTest {

    @ParameterizedTest(name = "#{index} - Run test with password = {0}")
    @MethodSource("validPasswordProvider")
    void passwordMustBeValid(String password) {
        // When
        Throwable throwable = catchThrowable(() -> ValidationUtils.requireValidPassword(password, "password"));

        // Then
        assertThat(throwable).isNull();
    }

    @ParameterizedTest(name = "#{index} - Run test with password = {0}")
    @MethodSource("invalidPasswordProvider")
    void passwordMustBeInValid(String password) {
        // When
        Throwable throwable = catchThrowable(() -> ValidationUtils.requireValidPassword(password, "password"));

        // Then
        assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    static Stream<String> validPasswordProvider() {
        return Stream.of(
                "AAAbbbccc*123",
                "Hello world-123",
                "A!@#&()*a1",               // test punctuation part 1
                "A[{}]:;',?/*a1",           // test punctuation part 2
                "A~$^_=<>a1",               // test symbols
                "01234-abcdefgAB",         // test 15 chars
                "123Aa*Aa"                  // test 8 chars
        );
    }

    // At least
    // one lowercase character,
    // one uppercase character,
    // one digit,
    // one special character
    // and length between 8 and 20.
    static Stream<String> invalidPasswordProvider() {
        return Stream.of(
                "12345678",                 // invalid, only digit
                "abcdefgh",                 // invalid, only lowercase
                "ABCDEFGH",                 // invalid, only uppercase
                "abc123$$$",                // invalid, at least one uppercase
                "ABC123$$$",                // invalid, at least one lowercase
                "ABC$$$$$$",                // invalid, at least one digit
                "java REGEX 123",           // invalid, at least one special character
                "java REGEX 123 %",         // invalid, % is not in the special character group []
                "________",                 // invalid
                "--------",                 // invalid
                " ",                        // empty
                "");                        // empty
    }

}