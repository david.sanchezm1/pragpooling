package com.davidsanchez.pragpooling.infrastructure.out.jpa.adapter;

import com.davidsanchez.pragpooling.domain.model.Neighborhood;
import com.davidsanchez.pragpooling.factory.NeighborhoodFactoryDataTest;
import com.davidsanchez.pragpooling.infrastructure.exception.NeighborhoodAlreadyExistsException;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.NeighborhoodEntity;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper.INeighborhoodEntityMapper;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.repository.INeighborhoodRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class NeighborhoodJpaAdapterTest {

    @InjectMocks
    NeighborhoodJpaAdapter neighborhoodJpaAdapter;

    @Mock
    INeighborhoodRepository neighborhoodRepository;

    @Mock
    INeighborhoodEntityMapper neighborhoodEntityMapper;

    @Test
    void mustSaveANeighborhood() {
        // Given
        Neighborhood neighborhood = NeighborhoodFactoryDataTest.getNeighborhood();

        // When
        when(neighborhoodRepository.findByName(anyString())).thenReturn(Optional.empty());
        neighborhoodJpaAdapter.saveNeighborhood(neighborhood);

        // Then
        verify(neighborhoodEntityMapper).toEntity(neighborhood);
    }

    @Test
    void mustThrowNeighborhoodAlreadyExistsException_When_NeighborhoodExists() {
        // Given
        Neighborhood neighborhood = NeighborhoodFactoryDataTest.getNeighborhood();
        NeighborhoodEntity neighborhoodEntity = new NeighborhoodEntity();

        // When
        when(neighborhoodRepository.findByName(anyString())).thenReturn(Optional.of(neighborhoodEntity));
        Throwable throwable = catchThrowable(() -> neighborhoodJpaAdapter.saveNeighborhood(neighborhood));

        // Then
        assertThat(throwable).isExactlyInstanceOf(NeighborhoodAlreadyExistsException.class);
    }
}