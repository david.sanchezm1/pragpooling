package com.davidsanchez.pragpooling.factory;

import com.davidsanchez.pragpooling.domain.model.AvailableRide;
import com.davidsanchez.pragpooling.domain.model.Ride;
import com.davidsanchez.pragpooling.domain.model.value_object.RouteGetRide;
import com.davidsanchez.pragpooling.util.DateUtil;

public class RideFactoryDataTest {

    private RideFactoryDataTest() {

    }

    public static Ride getRide() {
        Ride ride = new Ride();
        ride.setId(1L);
        ride.setStops(0);
        ride.setAvailableSeats(4);
        ride.setMeetingPoint("meeting point");
        ride.setDepartureTime(DateUtil.createTimestampTest());

        RouteGetRide route = new RouteGetRide();
        route.setId(1L);
        route.setDriverId(1L);
        route.setTotalPaths(1);
        ride.setRouteByRouteId(route);

        return ride;
    }

    public static AvailableRide getAvailableRide() {
        AvailableRide availableRide = new AvailableRide();
        availableRide.setDate(DateUtil.createTimestampTest());
        availableRide.setNeighborhoodOriginId(1L);
        availableRide.setNeighborhoodDestinationId(2L);
        return availableRide;
    }
}
