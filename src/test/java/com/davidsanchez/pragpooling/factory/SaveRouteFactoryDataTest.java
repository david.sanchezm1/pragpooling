package com.davidsanchez.pragpooling.factory;

import com.davidsanchez.pragpooling.domain.model.SaveRoute;
import com.davidsanchez.pragpooling.util.DateUtil;

import java.sql.Timestamp;
import java.util.List;

public class SaveRouteFactoryDataTest {

    private SaveRouteFactoryDataTest() {

    }

    public static SaveRoute getSaveRoute() {
        Timestamp timestamp = DateUtil.createTimestampTest();
        SaveRoute saveRoute = new SaveRoute();
        saveRoute.setSeats(2);
        saveRoute.setRides(List.of(timestamp));
        saveRoute.setNeighborhoods(List.of(1L, 2L));
        saveRoute.setDescription("New user route");
        saveRoute.setMeetingPoint("Meeting point 1");
        return saveRoute;
    }
}
