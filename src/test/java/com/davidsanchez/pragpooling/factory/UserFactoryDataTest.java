package com.davidsanchez.pragpooling.factory;

import com.davidsanchez.pragpooling.domain.model.CurrentUser;
import com.davidsanchez.pragpooling.domain.model.User;

public class UserFactoryDataTest {

    private UserFactoryDataTest() {

    }

    public static User getUser() {
        User user = new User();
        user.setId(1L);
        user.setFirstName("Tara");
        user.setLastName("Stokes");
        user.setEmail("tara@gmail.com");
        user.setAddress("1825 Muir Junction");
        user.setPhoneNumber("708-553-7102");
        user.setPassword("HelloWorld1234*");
        user.setUsername("Tara_85302192-a832-402f-a482-6a03f5fd403d");
        return user;
    }

    public static CurrentUser getCurrentUser() {
        CurrentUser currentUser = new CurrentUser();
        currentUser.setUsername("cognito_username");
        return currentUser;
    }
}
