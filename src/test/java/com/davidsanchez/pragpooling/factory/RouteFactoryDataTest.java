package com.davidsanchez.pragpooling.factory;

import com.davidsanchez.pragpooling.domain.model.Route;

public class RouteFactoryDataTest {

    private RouteFactoryDataTest() {

    }

    public static Route getRoute() {
        Route route = new Route();
        route.setId(1L);
        route.setDriverId(1L);
        return route;
    }
}
