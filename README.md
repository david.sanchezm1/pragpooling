<a name="readme-top"></a>
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/davidsanchez96/pragpooling">
    <img src="src/main/resources/static/logo.png" alt="Logo" width="340" height="130">
  </a>

<h3 align="center">PRAGPOOLING</h3>

  <p align="center">
    Carpooling system, in which
a driver will be able to record his trips (composed of
a route and a schedule), so that a passenger
can subscribe to this or other trips that serve you according to
your need. This in order to minimize the
number of vehicles in transit and maximize the
capacity of people who transport in them.
  </p>
</div>

### Built With

* ![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
* ![Spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
* ![Gradle](https://img.shields.io/badge/Gradle-02303A.svg?style=for-the-badge&logo=Gradle&logoColor=white)
* ![MySQL](https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white)


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these steps.

### Prerequisites

* JDK 11 [https://jdk.java.net/java-se-ri/11](https://jdk.java.net/java-se-ri/11)
* Gradle [https://gradle.org/install/](https://gradle.org/install/)

### Recommended Tools
* IntelliJ Community [https://www.jetbrains.com/idea/download/](https://www.jetbrains.com/idea/download/)
* Postman [https://www.postman.com/downloads/](https://www.postman.com/downloads/)

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/davidsanchez96/pragpooling.git
   ```
2. Change directory
   ```sh
   cd pragpooling
   ```
3. Create a new database in MySQL called pragpooling_david
4. Update the database connection settings 
   ```yml
   # src/main/resources/application.yml   
   spring:
      datasource:
          url: jdbc:mysql://localhost/pragpooling_david
          username: root
          password: 1234
   ```
5. Run the application using Gradle
   ```sh
   gradlew bootRun
   ```


<!-- USAGE -->
## Usage

1. First you must create Neighborhoods and Users, then you can create a route.

Option 1: Import the Pragpooling.postman_collection.json file in Postman

![](src/main/resources/static/postman.png)


Option 2: Open [http://localhost:8090/swagger-ui/index.html](http://localhost:8090/swagger-ui/index.html) in your web browser

![](src/main/resources/static/swagger.png)


After successfully login a user you will receive an idToken

![](src/main/resources/static/login.png)

All the endpoints except login and signup require the idToken to be sent in the Authorization header

![](src/main/resources/static/authorization-header.png)

If you are using the swagger ui, then you can press the Authorize button at the top and then paste the idToken

![](src/main/resources/static/swagger-auth.png)

<!-- ROADMAP -->
## Tests

- Open the project in IntelliJ

- Right click in the root folder

![](src/main/resources/static/test_1.png)

- After executing the tests with coverage you should see a new panel being open on the right hand side

![](src/main/resources/static/test_2.png)

<!-- ROADMAP -->
## Roadmap

- [X] User registration in the system (driver and passenger)
- [X] Log In (driver and passenger)
- [X] Trip creation (driver)
- [X] Consult trips (driver)
- [X] Find route (passenger)
- [X] Join a trip (passenger)


